/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import Routes from './app/navigation/index';
import {SafeAreaView} from 'react-native';

export default class App extends Component {
  render() {
    console.disableYellowBox = true;
    return (
      <SafeAreaView style={{flex: 1}}>
        <Routes />
      </SafeAreaView>
    );
  }
}
