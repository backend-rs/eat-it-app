import React, {Component} from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';
import styles from './style';
import {Menu} from 'react-native-paper';
import {CheckBox} from 'native-base';

export default class menuList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Menu
        style={styles.menu_position}
        contentStyle={styles.menu_background}
        visible={this.props.visible}
        onDismiss={() => this.props.close(this.props.id)}
        anchor={
          <TouchableOpacity onPress={() => this.props.open(this.props.id)}>
            <View
              style={[styles.row, styles.picker_style, styles.between_spacing]}>
              {this.props.id != 6 ? (
                <Text style={styles.picker_label}>
                  {this.props.selectedItem}
                  {this.props.id == 3
                    ? this.props.selectedItem == 1
                      ? ' day'
                      : ' days'
                    : ''}
                </Text>
              ) : this.props.selectedItem == '' ? (
                <Text style={styles.placeholder_text}>Select items</Text>
              ) : (
                <Text style={styles.picker_label}>
                  {this.props.selectedItem}
                </Text>
              )}
              <Image
                source={require('../../assets/down_arrow.png')}
                style={styles.picker_icons}
              />
            </View>
          </TouchableOpacity>
        }>
        {this.props.list.map(value => {
          let index = this.props.list.indexOf(value);
          return this.props.id != 6 ? (
            <Menu.Item
              titleStyle={[styles.menu_list_title]}
              style={styles.list_item_height}
              onPress={() => this.props.onSelect(value, this.props.id, '')}
              title={value.name}
            />
          ) : (
            <View style={[styles.row, styles.checkbox_container]}>
              <CheckBox
                checked={this.props.isChecked[index]}
                color="grey"
                onPress={() => this.props.onSelect(value, this.props.id, index)}
                style={styles.ckeckbox}
              />
              <Menu.Item
                titleStyle={[styles.menu_list_title]}
                style={[styles.checkbox_list]}
                onPress={() => this.props.onSelect(value, this.props.id, index)}
                title={value.name}
              />
            </View>
          );
        })}
      </Menu>
    );
  }
}
