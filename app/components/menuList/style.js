import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  menu_position: {
    marginTop: hp(5),
  },
  menu_background: {
    backgroundColor: '#ECECEC',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  picker_style: {
    width: wp(38),
    height: hp(3.8),
    borderRadius: 20,
    fontSize: 10,
    paddingHorizontal: wp(3.6),
    backgroundColor: colors.unselectedFilter,
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  picker_label: {
    fontSize: 13,
  },
  picker_icons: {
    height: hp(1.8),
    width: hp(1.8),
  },
  menu_list_title: {
    fontSize: 13,
    fontWeight: fonts.FONT_BOLD,
    color: colors.greyText,
  },
  list_item_height: {
    height: hp(4),
    paddingHorizontal: wp(0),
    width: wp(38),
  },
  placeholder_text: {color: 'grey', fontSize: 13, padding: 0, margin: 0},
  checkbox_container: {
    alignItems: 'center',
    width: wp(39.4),
  },
  ckeckbox: {
    marginRight: wp(3),
    borderRadius: 4,
    height: hp(2.6),
    width: hp(2.6),
  },
  checkbox_list: {
    height: hp(4),
    paddingHorizontal: wp(0),
  },
};
export default styles;
