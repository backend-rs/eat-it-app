import React, {Component} from 'react';
import styles from '../Loader/style';
import * as colors from '../../constants/colors';
import {View, ActivityIndicator} from 'react-native';
export default class loader extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.loader}>
        <ActivityIndicator
          animating={this.props.isVisibleLoading}
          size="large"
          color={colors.primaryColor}
        />
      </View>
    );
  }
}
