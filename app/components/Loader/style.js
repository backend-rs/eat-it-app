const styles = {
  loader: {
    position: 'absolute',
    top: '50%',
    right: 0,
    left: 0,
  },
};
export default styles;
