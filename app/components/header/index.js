import React, {Component} from 'react';
import {Image} from 'react-native';
import styles from './style';
import {Header, Left, Right, Title} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default class header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'products',
      isLoginModalVisible: false,
      isAbortProcessModalVisible: false,
    };
  }

  render() {
    return (
      <Header style={styles.headerView}>
        <Left>
          <TouchableOpacity onPress={this.props.onBack}>
            <Image
              resizeMode="contain"
              source={require('../../assets/back_arrow.png')}
              style={styles.arrow}
            />
          </TouchableOpacity>
        </Left>
        <Title allowFontScaling={false} style={styles.HeaderTitle}>
          {this.props.title}
        </Title>
        <Right />
      </Header>
    );
  }
}
