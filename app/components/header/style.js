import {StyleSheet, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';
import * as colors from '../../constants/colors';
import * as fonts from '../../constants/fonts';

const styles = StyleSheet.create({
  headerView: {backgroundColor: colors.headerColor,elevation:0},
  wrapper: {flex: 1},
  headerBtn: {marginLeft: '8%'},
  HeaderTitle: {
    color: colors.whiteColor,
    fontSize: fonts.FONT_HEADING,
    fontWeight: fonts.FONT_BOLD,
    justifyContent: 'center',
    alignSelf: 'center',
    ...Platform.select({
      ios: {
        marginLeft: 0,
      },
      android: {
        marginLeft: '20%',
      },
    }),
  },
  arrow: {
    height: hp(4.5),
    width: hp(4.5),
  },
});
export default styles;
