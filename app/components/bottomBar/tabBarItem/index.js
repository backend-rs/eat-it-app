import React, {Component, useEffect, useRef, useMemo} from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Image,
  Animated,
} from 'react-native';
import styles from './style';
import {item} from './upString';
import DiagonalTransition from '../diagonalTransition';

const TabItem = props => {
  // console.log('propsssss:::',props)
  const animation = item({to: props.active ? 1 : 0}, {stiffness: 50});
  const labelTranslate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [20, 0],
  });
  const iconTranslate = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -30],
  });
  const labelVisibility = animation;
  const iconVisibility = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 0],
  });
  const dotScale = animation;
  return (
    <TouchableWithoutFeedback onPress={props.onPress}>
      <View style={[styles.container, props.style]}>
        <Animated.View
          style={[
            styles.centered,
            {
              opacity: labelVisibility,
              transform: [{translateY: labelTranslate}],
            },
          ]}>
          <DiagonalTransition visibility={labelVisibility}>
            <Text style={styles.label}>{props.label}</Text>
          </DiagonalTransition>
        </Animated.View>
        <Animated.View
          style={[
            styles.centered,
            {opacity: iconVisibility, transform: [{translateY: iconTranslate}]},
          ]}>
          <DiagonalTransition visibility={iconVisibility}>
            <Image style={styles.icon} source={props.icon} />
          </DiagonalTransition>
        </Animated.View>
        <Animated.View style={[styles.dot, {transform: [{scale: dotScale}]}]} />
      </View>
    </TouchableWithoutFeedback>
  );
};
export default TabItem;
