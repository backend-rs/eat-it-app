import {
  heightPercentageToDP as hp,
} from '../../../utility/index';
import { Platform } from 'react-native';

const styles = {
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  centered: {
    position: 'absolute',
  },
  icon: {
    height: Platform.OS == 'ios' ? hp(2.75) : hp(3.6),
    width: Platform.OS == 'ios' ? hp(2.75) : hp(3.6),
  },
  label: {
    color: 'white',
    fontSize: 13,
    fontWeight: '600',
    letterSpacing: -0.2,
  },
  dot: {
    position: 'absolute',
    bottom: 8,
    width: 5,
    height: 5,
    borderRadius: 2.5,
    backgroundColor: 'white',
  },
};
export default styles;
