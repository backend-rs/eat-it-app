// import React, { Component } from 'react';
import React, {Component} from 'react';

import { View, TouchableOpacity, Image, Platform } from 'react-native';
import styles from './style';
import TabItem from '../tabBarItem';
import * as colors from '../../../constants/colors';
import LinearGradient from 'react-native-linear-gradient';
import { NavigationActions, StackActions } from 'react-navigation';

export default class tabBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {
          icon: require('../../../assets/home.png'),
          label: 'Home',
        },
        {
          icon: require('../../../assets/search.png'),
          label: 'Search',
        },
        {
          icon: require('../../../assets/share_icon.png'),
          label: '',
        },
        {
          icon: require('../../../assets/my_food.png'),
          label: 'My food',
        },
        {
          icon: require('../../../assets/profile.png'),
          label: 'Profile',
        },
      ],
      activeIndex: 0,
    };
  }
  componentDidMount = async () => {
    const { navigation } = this.props;
    const { index: activeRouteIndex } = navigation.state;
    await this.setState({ activeIndex: activeRouteIndex });
  };
  onTab = async (index, router) => {
    // console.log('indexxxxxxxxxxxxx', index);
    await this.setState({ activeIndex: index });
    index != 2
      ? await this.props.navigation.navigate(`${router.key}`)
      : await this.props.navigation.navigate('AttatchFoodPhotos');
  };
  render() {
    const { navigation } = this.props;
    const { routes, index: activeRouteIndex } = navigation.state;
    let router = routes[activeRouteIndex];
    // console.log('routesssssssssssssssssss:', routes);
    // console.log('active route index:', activeRouteIndex);
    // console.log('route ', router);
    return (
      Platform.OS == 'ios' ?
        <View
          style={styles.main_ios}
        >
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={[colors.gradientFirstColor, colors.gradientSecondColor]}
            style={styles.bar_ios}>
          </LinearGradient>
          {this.state.items.map((it, index) =>
            index != 2 ? (
              <TabItem
                key={index}
                style={styles.item}
                icon={it.icon}
                label={it.label}
                active={index === this.state.activeIndex}
                onPress={() => this.onTab(index, routes[index])}
              />
            ) : (
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => this.onTab(index, routes[index])}>
                  <View style={styles.add_container_ios}>
                    <Image source={it.icon} style={styles.icon_ios} />
                  </View>
                </TouchableOpacity>
              ),
          )}
        </View> : <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={[colors.gradientFirstColor, colors.gradientSecondColor]}
        style={styles.bar}>
        {this.state.items.map((it, index) =>
          index != 2 ? (
            <TabItem
              key={index}
              style={styles.item}
              icon={it.icon}
              label={it.label}
              active={index === this.state.activeIndex}
              onPress={() => this.onTab(index, routes[index])}
            />
          ) : (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => this.onTab(index, routes[index])}>
              <View style={styles.add_container}>
                <Image source={it.icon} style={styles.icon} />
              </View>
            </TouchableOpacity>
          ),
        )}
      </LinearGradient>

    );
  }
}
