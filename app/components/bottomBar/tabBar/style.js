import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../utility/index';

const styles = {
  bar: {
    flexDirection: 'row',
    height: 50,
    borderTopColor: 'transparent',
    borderRadius: 40,
    width: wp(94),
    alignSelf: 'center',
    elevation: 6,
    position:'absolute',
    bottom:10
  },
  item: {
    flex: 1,
  },
  add_container: {
    height: hp(8),
    width: hp(8),
    borderRadius: hp(8) / 2,
    elevation: 4,
    borderRadius: hp(8) / 2,
    marginTop: hp(-3.4),
    zIndex: 1,
  },
  icon: {
    // zIndex: 1,
    height: hp(8),
    width: hp(8),
  },





  main_ios: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    backgroundColor: 'white',
    zIndex: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.2,
    shadowRadius: 10,
    elevation: 7,
    flexDirection: 'row',
    height: 50,
    borderTopColor: 'transparent',
    width: wp(94),
    marginLeft: wp(3),
    bottom: 10,
    overflow: 'visible',
    borderRadius: 40
  },
  bar_ios: {
    flexDirection: 'row',
    height: 50,
    borderTopColor: 'transparent',
    borderRadius: 40,
    width: wp(94),
    elevation: 6,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    backgroundColor: 'white',
    zIndex: -1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.2,
    shadowRadius: 10,
    // overflow: 'visible',
    marginLeft: wp(3),

  },
  add_container_ios: {
    height: hp(8),
    width: hp(8),
    borderRadius: hp(8) / 2,
    elevation: 4,
    borderRadius: hp(8) / 2,
    marginTop: hp(-3.4),
    zIndex: 2,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 7,
  },
  icon_ios: {
    // zIndex: 1,
    height: hp(6),
    width: hp(6)
  },
};
export default styles;
