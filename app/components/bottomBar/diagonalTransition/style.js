const styles = {
  container: {
    overflow: 'hidden',
  },
  coverContainer: {
    position: 'absolute',
    top: '120%',
    width: '100%',
  },
  cover: {
    height: 40,
    transform: [{skewY: '10deg'}],
  },
};
export default styles;
