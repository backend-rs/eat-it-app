import React, {Component} from 'react';
import {View, Animated} from 'react-native';
import styles from './style';

export default class diagonalTransition extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {} = styles;
    const translateY = this.props.visibility.interpolate({
      inputRange: [0, 1],
      outputRange: [-30, 0],
    });
    return (
      <View style={[styles.container, this.props.style]}>
        {this.props.children}
        <Animated.View
          style={[styles.coverContainer, {transform: [{translateY}]}]}>
          <View style={[styles.cover, {backgroundColor: '#D6A646'}]} />
        </Animated.View>
      </View>
    );
  }
}
