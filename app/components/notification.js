import * as utility from '../utility';
import firebase from 'react-native-firebase';

export const pushNotification = async handleNotification => {
  //we check if user has granted permission to receive push notifications.
  checkPermission();
  // Register all listener for notification
  createNotificationListeners(handleNotification);
};

const checkPermission = async () => {
  const enabled = await firebase.messaging().hasPermission();
  // If Premission granted proceed towards token fetch
  if (enabled) {
    getToken();
  } else {
    // If permission hasn’t been granted to our app, request user in requestPermission method.
    requestPermission();
  }
};

const getToken = async () => {
  let fcmToken = await utility.getItem('fcmToken');
  // console.log('getToken::fcmToken', fcmToken);
  if (!fcmToken) {
    firebase
      .messaging()
      .getToken()
      .then(async token => {
        console.log('fcmToken', token);
        await utility.setItem('fcmToken', token);
        // console.log('getToken::fcmToken', await utility.getItem('fcmToken'));
      });
    // fcmToken = await firebase.messaging().getToken();
    // if (fcmToken) {
    //   // user has a device token
    //   console.log('fcmToken', fcmToken);
    //   let fcmToken = await utility.setItem('fcmToken');
    // }
  }
};

const requestPermission = async () => {
  try {
    await firebase.messaging().requestPermission();
    // User has authorised
    getToken();
  } catch (error) {
    // User has rejected permissions
    console.log('permission rejected');
  }
};

const createNotificationListeners = async handleNotification => {
  // This listener triggered when notification has been received in foreground
  let notiCount = await utility.getItem('notificationCount');
  console.log('start notiCount:::', notiCount);

  notificationListener = firebase
    .notifications()
    .onNotification(async notification => {
      const channel = new firebase.notifications.Android.Channel(
        'channelId',
        'Channel Name',
        firebase.notifications.Android.Importance.Max,
      ).setDescription('A natural description of the channel');
      firebase.notifications().android.createChannel(channel);
      notification.android.setChannelId('channelId');
      notification.android.setAutoCancel(true);
      const {data} = notification;

      if (data.notificationType == 'Chat') {
        let tempChatData = JSON.parse(data.chatData);
        let activeChat = await utility.getItem('activeChat');

        if (activeChat == null || activeChat == undefined || activeChat == '') {
          firebase.notifications().displayNotification(notification);
        } else if (activeChat != tempChatData.orderId) {
          firebase.notifications().displayNotification(notification);
        }
      } else {
        firebase.notifications().displayNotification(notification);
      }
      if (notiCount == null || notiCount == undefined || notiCount == '') {
        notiCount = 1;
      } else {
        notiCount += 1;
      }
      await utility.setItem('notificationCount', notiCount);
    });

  // This listener triggered when app is in backgound and we click, tapped and opened notifiaction
  notificationOpenedListener = firebase
    .notifications()
    .onNotificationOpened(async notificationOpen => {
      const {title, body, data} = notificationOpen.notification;

      handleNotification(data);
      if (notiCount == null || notiCount == undefined || notiCount == '') {
        notiCount = 0;
      } else if (notiCount > 0) {
        notiCount -= 1;
      } else {
        notiCount = 0;
      }
      await utility.setItem('notificationCount', notiCount);
    });

  // This listener triggered when app is closed and we click,tapped and opened notification
  const notificationOpen = await firebase
    .notifications()
    .getInitialNotification();
  if (notificationOpen) {
    const {title, body, data} = notificationOpen.notification;

    handleNotification(data);
    // displayNotification(data.title, data.body);
    if (notiCount == null || notiCount == undefined || notiCount == '') {
      notiCount = 0;
    } else if (notiCount > 0) {
      notiCount -= 1;
    } else {
      notiCount = 0;
    }
    await utility.setItem('notificationCount', notiCount);
  }
};
