import ImageCropPicker from 'react-native-image-crop-picker';
import ImagePicker from 'react-native-image-picker';

export const onImageCrop = async data => {
  try {
    let image = await ImageCropPicker.openCropper({
      path: data.uri,
      width: 300,
      height: 400,
      cropping: true,
    });
    let response = {
      uri: image.path,
      fileName: data.fileName,
      type: data.type,
    };
    return response;
  } catch (error) {
    console.log('cropper another error::', error);
  }
};
export const onImagePicker = async () => {
  let options = {
    cameraType: 'front',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  return new Promise((resolve, reject) => {
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        utility.alert(response.customButton);
      } else {
        const source = {uri: response.uri};
        let imageData = {
          uri: response.uri,
          fileName: response.fileName,
          type: response.type,
        };
        resolve(imageData);
      }
    });
  });
};












// onLaunchCamera = () => {
//     let options = {
//       cameraType: 'front',
//       storageOptions: {
//         skipBackup: true,
//         path: 'images',
//       },
//     };
//     ImagePicker.showImagePicker(options, response => {
//       if (response.didCancel) {
//         console.log('User cancelled image picker');
//       } else if (response.error) {
//         console.log('ImagePicker Error: ', response.error);
//       } else if (response.customButton) {
//         console.log('User tapped custom button: ', response.customButton);
//         utility.alert(response.customButton);
//       } else {
//         const source = {uri: response.uri};
//         let file = {
//           uri: response.uri,
//           fileName: response.fileName,
//           type: response.type,
//         };
//         this.setState({isImagePicked: true, file: file});
//       }
//     });
//   };