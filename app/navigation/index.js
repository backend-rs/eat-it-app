import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator, BottomTabBar} from 'react-navigation-tabs';

import Loader from '../components/Loader';
import Splash from '../screen/splash';
import Login from '../screen/login';
import ForgotPassword from '../screen/forgotPassword';
import Home from '../screen/home';
import Profile from '../screen/profile';
import Search from '../screen/search';
import Filter from '../screen/filter';
import AttatchFoodPhotos from '../screen/attatchFoodPhotos';
import FoodDetails from '../screen/foodDetails';
import MyFood from '../screen/myFood';
import SearchName from '../screen/searchName';
import LikeDislikeFood from '../screen/likeDislikeFood';
import ShareFood from '../screen/shareFood';
import Messages from '../screen/messages';
import ChatScreen from '../screen/chatScreen';
import Orders from '../screen/orders';
import OrderDetails from '../screen/orderDetails';
import ConfirmOrder from '../screen/confirmOrder';
import FollowedSellers from '../screen/followedSellers';
import Notifications from '../screen/notifications';
import Communication from '../screen/communication';
import About from '../screen/about';
import CustomerCare from '../screen/customerCare';
import ConfirmPayment from '../screen/confirmPayment';
import Map from '../screen/map';
import AddNewFood from '../screen/addNewFood';
import TermsAndConditions from '../screen/termsAndConditions';

import TabItem from '../components/bottomBar/tabBarItem';
import TabBar from '../components/bottomBar/tabBar';
import DiagonalTransition from '../components/bottomBar/diagonalTransition';
import Header from '../components/header';
import MenuList from '../components/menuList';
import Payment from '../screen/payment';

const TabNavigator = createBottomTabNavigator(
  {
    tab1: {
      screen: Home,
    },
    tab2: {
      screen: Search,
    },
    tab3: {
      screen: AttatchFoodPhotos,
    },
    tab4: {
      screen: MyFood,
    },
    tab5: {
      screen: Profile,
    },
  },
  {
    tabBarComponent: props => <TabBar {...props} />,
    tabBarOptions: {
      activeTintColor: '#eeeeee',
      inactiveTintColor: '#222222',
    },
    lazy: true,
  },
);

const AppStack = createStackNavigator(
  {
    Loader: {
      screen: Loader,
      navigationOptions: {
        headerShown: false,
      },
    },
    Splash: {
      screen: Splash,
      navigationOptions: {
        headerShown: false,
      },
    },

    Login: {
      screen: Login,
      navigationOptions: {
        headerShown: false,
      },
    },

    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: {
        headerShown: false,
      },
    },
    Home: {
      screen: Home,
      navigationOptions: {
        headerShown: false,
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        headerShown: false,
      },
    },
    Search: {
      screen: Search,
      navigationOptions: {
        headerShown: false,
      },
    },
    Filter: {
      screen: Filter,
      navigationOptions: {
        headerShown: false,
      },
    },
    AttatchFoodPhotos: {
      screen: AttatchFoodPhotos,
      navigationOptions: {
        headerShown: false,
      },
    },
    FoodDetails: {
      screen: FoodDetails,
      navigationOptions: {
        headerShown: false,
      },
    },
    MyFood: {
      screen: MyFood,
      navigationOptions: {
        headerShown: false,
      },
    },
    SearchName: {
      screen: SearchName,
      navigationOptions: {
        headerShown: false,
      },
    },
    LikeDislikeFood: {
      screen: LikeDislikeFood,
      navigationOptions: {
        headerShown: false,
      },
    },
    ShareFood: {
      screen: ShareFood,
      navigationOptions: {
        headerShown: false,
      },
    },

    Header: {
      screen: Header,
      navigationOptions: {
        headerShown: false,
      },
    },
    Messages: {
      screen: Messages,
      navigationOptions: {
        headerShown: false,
      },
    },
    ChatScreen: {
      screen: ChatScreen,
      navigationOptions: {
        headerShown: false,
      },
    },

    Orders: {
      screen: Orders,
      navigationOptions: {
        headerShown: false,
      },
    },
    OrderDetails: {
      screen: OrderDetails,
      navigationOptions: {
        headerShown: false,
      },
    },
    ConfirmOrder: {
      screen: ConfirmOrder,
      navigationOptions: {
        headerShown: false,
      },
    },
    FollowedSellers: {
      screen: FollowedSellers,
      navigationOptions: {
        headerShown: false,
      },
    },
    Notifications: {
      screen: Notifications,
      navigationOptions: {
        headerShown: false,
      },
    },
    Communication: {
      screen: Communication,
      navigationOptions: {
        headerShown: false,
      },
    },
    About: {
      screen: About,
      navigationOptions: {
        headerShown: false,
      },
    },
    CustomerCare: {
      screen: CustomerCare,
      navigationOptions: {
        headerShown: false,
      },
    },
    TermsAndConditions: {
      screen: TermsAndConditions,
      navigationOptions: {
        headerShown: false,
      },
    },
    ConfirmPayment: {
      screen: ConfirmPayment,
      navigationOptions: {
        headerShown: false,
      },
    },
    Map: {
      screen: Map,
      navigationOptions: {
        headerShown: false,
      },
    },
    AddNewFood: {
      screen: AddNewFood,
      navigationOptions: {
        headerShown: false,
      },
    },
    Payment: {
      screen: Payment,
      navigationOptions: {
        headerShown: false,
      },
    },
    TabItem: {
      screen: TabItem,
      navigationOptions: {
        headerShown: false,
      },
    },
    TabBar: {
      screen: TabBar,
      navigationOptions: {
        headerShown: false,
      },
    },
    DiagonalTransition: {
      screen: DiagonalTransition,
      navigationOptions: {
        headerShown: false,
      },
    },
    MenuList: {
      screen: MenuList,
      navigationOptions: {
        headerShown: false,
      },
    },
    BottomTab: {
      screen: TabNavigator,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'Splash',
    headerMode: 'none',
    mode: 'modal',
  },
);

const Routes = createAppContainer(
  createSwitchNavigator({
    App: AppStack,
    BottomTab: TabNavigator,
  }),
);

export default Routes;
