import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  button_container: {
    height: hp(5.4),
    width: wp(32),
    borderRadius: 20,
    marginTop: hp(2),
    alignSelf: 'flex-end',
    elevation: 4,
  },
  content_container_style: {
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  button_text: {
    color: 'white',
  },
  centered_text: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  scroll_container: {
    borderColor: colors.primaryColor,
    borderWidth: 1.8,
    borderRadius: 8,
    alignSelf: 'center',
    height: hp(30),
  },
  colored_text: {
    color: colors.primaryColor,
  },
  timing_heading_style: {
    fontSize: fonts.FONT_TEXT,
  },
  type_text: {
    color: colors.greyText,
    fontSize: 12,
    textTransform: 'capitalize',
  },
  price: {
    fontSize: 18,
    fontWeight: fonts.FONT_BOLD,
  },
  address_style: {
    fontSize: fonts.FONT_TEXT,
    color: colors.greyText,
  },
  bottom_spacing: {
    marginBottom: hp(2.2),
  },
  product_name: {
    fontSize: 20,
    fontWeight: fonts.FONT_BOLD,
    textTransform: 'capitalize',
  },
  detail_container: {
    width: wp(85),
    alignSelf: 'center',
  },
  bottom_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    width: wp(86),
    marginBottom: hp(3),
  },
  spacing: {
    marginTop: hp(2.2),
    // marginBottom: hp(3.2),
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  free_text: {
    color: 'green',
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  top_spacing: {
    marginTop: hp(4),
  },
  id_heading: {
    fontSize: 21,
    textTransform: 'capitalize',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  otp_fields: {
    marginVertical: hp(2.4),
    width: wp(55),
    alignSelf: 'center',
  },
  otp_input_box: {
    padding: 0,
    height: hp(5.4),
    width: hp(5.4),
    borderRadius: 4,
    backgroundColor: '#696969',
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
  },
  background_theme_color: {
    backgroundColor: colors.primaryColor,
  },
  otp_container: {
    marginVertical: hp(1),
  },
  text_style: {
    fontSize: fonts.FONT_TEXT,
  },
  otp_card: {
    height: hp(12),
    width: wp(86),
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: hp(2),
  },
  otp_text: {
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: wp(6),
  },
  status_text: {
    fontSize: 18,
    textTransform: 'capitalize',
    fontWeight: fonts.FONT_BOLD,
  },
  rating_button_container: {
    height: hp(5.4),
    width: wp(45),
    borderRadius: 20,
    marginVertical: hp(2),
    alignSelf: 'flex-end',
    elevation: 4,
  },
  logo_icons: {
    height: hp(2),
    width: hp(2),
    marginRight: hp(1),
  },
  dot_style: {
    width: 15,
    height: 15,
    borderRadius: 15 / 2,
    backgroundColor: colors.primaryColor,
    marginTop: hp(-2.1),
  },
  inactive_dot_styles: {
    backgroundColor: '#696969',
    width: 22,
    height: 22,
    borderRadius: 22 / 2,
  },
  images: {
    height: '100%',
    width: '98.8%',
    borderRadius: 6,
    alignSelf: 'center',
    marginLeft: wp(-1),
  },

  container: {
    width: '100%',
    height: '100%',
  },
  quantity_text_style: {
    fontSize: 16,
  },
};
export default styles;
