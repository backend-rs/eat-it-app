import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  list_height: {
    height: hp(86),
  },
  content_container: {
    width: wp(92.8),
    alignSelf: 'center',
  },
};
export default styles;
