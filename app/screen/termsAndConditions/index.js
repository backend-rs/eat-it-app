import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import styles from './style';
import Header from '../../components/header';
import Loader from '../../components/Loader';

export default class termsAndConditions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisibleLoading: true,
      url: '',
    };
  }
  onBack = async () => {
    await this.props.navigation.goBack();
  };
  componentDidMount = async () => {
    await this.setState({
      url: 'http://54.179.71.251:6600/eat-it/terms-and-conditions',
    });
    setTimeout(async () => {
      await this.setState({isVisibleLoading: false});
    }, 500);
  };

  render() {
    const {list_height, content_container} = styles;
    const INJECTEDJAVASCRIPT = `const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=0.5, maximum-scale=0.3, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `;
    return (
      <>
        <Header title={'Terms and conditions'} onBack={this.onBack} />
        <WebView
          style={[list_height, content_container, {backgroundColor: '#F2F2F2'}]}
          source={{
            uri: this.state.url,
          }}
          injectedJavaScript={INJECTEDJAVASCRIPT}
        />
        <Loader isVisibleLoading={this.state.isVisibleLoading} />
      </>
    );
  }
}
