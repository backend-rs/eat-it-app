import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  container: {
    width: '100%',
    height: '92%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  list_height: {
    height: hp(86),
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  centered_text: {justifyContent: 'center', alignItems: 'center'},
  inner_container: {
    width: wp(96),
    alignSelf: 'center',
  },
  spacing: {
    marginTop: hp(2.4),
    // marginBottom: hp(3.8),
  },
  profile_image: {
    height: hp(7.4),
    width: hp(7.4),
    borderRadius: hp(7.4) / 2,
    alignSelf: 'center',
    borderColor: colors.primaryColor,
    borderWidth: 1.4,
    marginRight: hp(3),
    elevation: 4,
    backgroundColor: '#EDEDED',
  },
  like_icon: {
    height: hp(3.6),
    width: hp(3.6),
  },
  text_style: {
    fontSize: fonts.FONT_NORMAL,
    color: colors.greyText,
  },
  list_text_style: {
    justifyContent: 'space-between',
    marginVertical: hp(0.6),
    width: wp(50),
  },
  bottom_margin: {
    marginBottom: hp(3.4),
  },
  name_heading: {
    fontSize: fonts.FONT_TEXT,
    fontWeight: fonts.FONT_BOLD,
    textTransform: 'capitalize',
  },
};
export default styles;
