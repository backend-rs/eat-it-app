import React, {Component} from 'react';
import {View, ScrollView, Image, Text, TouchableOpacity} from 'react-native';
import styles from './style';
import {Accordion} from 'native-base';
import Icon from 'react-native-vector-icons/Feather';
import LikeDislikeFood from '../likeDislikeFood';
import * as Service from '../../api/services';
import * as utility from '../../utility/index';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {NavigationActions, StackActions} from 'react-navigation';
import moment from 'moment';
import Loader from '../../components/Loader';
import Header from '../../components/header';

export default class myFood extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from: '',
      visible: false,
      query: '',
      isDialogVisible: false,
      userId: '',
      userToken: '',
      foodId: '',
      sellerId: '',
      dataArray: [{id: '1', content: []}, {id: '2', content: []}],
      filtersList: [
        {
          name: 'Current',
        },
        {
          name: 'Past',
        },
      ],
      selectedIndex: 0,
      isSkipped: false,
      isVisibleLoading: false,
    };
  }
  componentDidMount = async () => {
    let isSkipped = await utility.getItem('isSkipped');
    await this.setState({isSkipped: isSkipped, selectedIndex: 0});

    if (this.state.isSkipped == false) {
      if (this.props.navigation.state.params) {
        let from;
        if (
          this.props.navigation.state.params.from != '' ||
          this.props.navigation.state.params.from != undefined ||
          this.props.navigation.state.params.from != null
        ) {
          from = this.props.navigation.state.params.from;
          await this.setState({from: from});
        }
      } else {
        await this.setState({from: ''});
      }
      const userId = await utility.getItem('userId');
      const userToken = await utility.getToken('token');
      await this.setState({
        userId: userId,
        userToken: userToken,
        query: `buyerId=${userId}&history=false`,
      });
      await this.getPurchasedFood();
      await this.getSharedFood();
    } else {
      await utility.showAlert('Please login first.', this.onLogin);
      await this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'BottomTab'})],
        }),
      );
      await this.props.navigation.navigate('tab1', {from: 'myFood'});
    }
  };
  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }
  onLogin = async () => {
    await this.props.navigation.navigate('Login');
  };
  onTabItem = async index => {
    if (index == 0) {
      await this.setState({
        query: `buyerId=${this.state.userId}&history=false`,
      });
    }
    if (index == 1) {
      await this.setState({
        query: `buyerId=${this.state.userId}&history=true`,
      });
    }
    await this.getPurchasedFood();
    await this.setState({selectedIndex: index});
  };
  onRefersh = async () => {
    await this.setState({query: `buyerId=${this.state.userId}&history=true`});
    await this.getPurchasedFood();
  };
  getPurchasedFood = async () => {
    await this.setState({isVisibleLoading: true});
    try {
      let response = Service.getDataApi(
        `orders?${this.state.query}`,
        this.state.userToken,
      );
      response
        .then(res => {
          if (res.data) {
            if (res.data.orders) {
              let shared = [{id: '1', content: []}, {id: '2', content: []}];
              let tempContent = [];
              if (res.data.orders.length != 0) {
                for (let i = 0; i < res.data.orders.length; i++) {
                  let image;
                  if (res.data.orders[i].images) {
                    image = res.data.orders[i].images[0].resize_url;
                  }
                  tempContent.push({
                    id: res.data.orders[i].id,
                    foodId: res.data.orders[i].foodId,
                    name: res.data.orders[i].name,
                    price: res.data.orders[i].price,
                    time: res.data.orders[i].availabilityTime,
                    quantity: res.data.orders[i].quantity,
                    address: res.data.orders[i].address.line,
                    image: image,
                    isLiked: res.data.orders[i].favoriteType,
                    type: res.data.orders[i].type,
                    status: res.data.orders[i].status,
                    foodStatus: res.data.orders[i].foodStatus,
                  });
                }
                shared[0].content = tempContent;
                shared[1].content = this.state.dataArray[1].content;
                this.setState({dataArray: shared, isVisibleLoading: false});
              } else {
                shared[0].content = [];
                shared[1].content = this.state.dataArray[1].content;
                this.setState({dataArray: shared, isVisibleLoading: false});
              }
            }
          } else {
            console.log('no data found', res.error);
            this.setState({isVisibleLoading: false});
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  getSharedFood = async () => {
    await this.setState({isVisibleLoading: true});
    try {
      let response = Service.getDataApi(
        `foods?userId=${this.state.userId}`,
        '',
      );
      response
        .then(res => {
          if (res.data) {
            let shared = [{id: '1', content: []}, {id: '2', content: []}];
            let tempContent = [];
            if (res.data.length != 0) {
              for (let i = 0; i < res.data.length; i++) {
                let image;
                if (res.data[i].images) {
                  image = res.data[i].images[0].resize_url;
                }
                tempContent.push({
                  id: res.data[i]._id,
                  name: res.data[i].name,
                  type: res.data[i].type,
                  price: res.data[i].price,
                  time: res.data[i].availabilityTime,
                  quantity: res.data[i].quantity,
                  address: res.data[i].address.line,
                  image: image,
                  isLiked: 'none',
                  type: res.data[i].type,
                  isVeg: true,
                  status: 'null',
                  foodStatus: 'inActive',
                });
              }
              shared[0].content = this.state.dataArray[0].content;
              shared[1].content = tempContent;
              this.setState({dataArray: shared, isVisibleLoading: false});
            } else {
              shared[0].content = this.state.dataArray[0].content;
              shared[1].content = [];
              this.setState({dataArray: shared, isVisibleLoading: false});
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };

  onOrderDetail = async (orderId, status, isToday, isLiked) => {
    await this.props.navigation.navigate('OrderDetails', {
      from: 'myFood',
      status: status,
      orderId: orderId,
      isLiked: isLiked,
      onRefersh: this.onRefersh,
    });
  };
  showFoodDetail = async data => {
    await this.props.navigation.navigate('FoodDetails', {
      foodId: data.id,
      from: 'myFood',
    });
  };
  renderHeader = (item, expanded) => {
    let index;
    if (this.state && this.state.dataArray) {
      index = this.state.dataArray.indexOf(item);
    }
    return (
      <View
        style={[
          index % 2 == 0 ? styles.even_color : styles.odd_color,
          styles.row,
          styles.between_spacing,
          styles.inner_container,
          styles.list_item_style,
          styles.list_spacing,
        ]}>
        <Text style={styles.list_item_text}>
          {' '}
          {index == 0 ? 'Food you bought' : 'Food you share'}
        </Text>
        {expanded ? (
          <Icon style={styles.top_down_icon} name="chevron-up" />
        ) : (
          <Icon style={styles.top_down_icon} name="chevron-down" />
        )}
      </View>
    );
  };

  renderContent = item => {
    let index;
    if (this.state && this.state.dataArray) {
      index = this.state.dataArray.indexOf(item);
    }
    return (
      <>
        <View style={styles.list_height} key={item.id}>
          {index == 0 ? (
            <View
              style={[
                styles.row,
                styles.filter_container,
                styles.between_spacing,
              ]}>
              {this.state.filtersList.map(item => {
                let index = this.state.filtersList.indexOf(item);
                return (
                  <TouchableOpacity onPress={() => this.onTabItem(index)}>
                    <View
                      style={
                        this.state.selectedIndex == index
                          ? [styles.filters, styles.selected_color]
                          : [styles.filters, styles.unselected_color]
                      }>
                      <Text style={styles.filter_text}>{item.name}</Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </View>
          ) : (
            <View />
          )}
          {item.content.length != 0 ? (
            <ScrollView>
              {item.content.map(value => {
                return (
                  <TouchableOpacity
                    onPress={
                      index == 0
                        ? () => {
                            this.onOrderDetail(
                              value.id,
                              value.status,
                              value,
                              value.isLiked,
                            );
                          }
                        : () => {
                            this.showFoodDetail(value);
                          }
                    }>
                    <View
                      style={[
                        styles.row,
                        styles.column_between_spaceing,
                        styles.inner_container,
                        styles.inner_list_spacing,
                      ]}>
                      <View style={styles.row}>
                        <View style={styles.list_image_continer}>
                          <Image
                            resizeMode="cover"
                            source={{uri: value.image}}
                            style={styles.list_image}
                          />
                        </View>
                        <View
                          style={[
                            styles.column,
                            styles.column_between_spaceing,
                          ]}>
                          <View style={{width: widthPercentageToDP(45)}}>
                            <View style={[styles.row]}>
                              <Text style={styles.product_heading}>
                                {value.name}
                              </Text>
                            </View>
                            <Text style={styles.address_text} numberOfLines={1}>
                              {value.address}
                            </Text>
                          </View>

                          <View style={[styles.row, styles.between_spacing]}>
                            <View style={[styles.row, styles.row_center_align]}>
                              <Image
                                style={styles.logo_icons}
                                source={
                                  value.type == 'veg'
                                    ? require('../../assets/veg_icon.png')
                                    : value.type == 'langar'
                                    ? require('../../assets/langar.png')
                                    : require('../../assets/nonveg_icon.png')
                                }
                              />
                              <Text style={styles.text_style}>
                                {value.type == 'veg'
                                  ? 'Veg'
                                  : value.type == 'langar'
                                  ? 'Langar'
                                  : 'Non-veg'}
                              </Text>
                            </View>
                            <View style={[styles.row, styles.row_center_align]}>
                              <Image
                                resizeMode="stretch"
                                source={
                                  value.type == 'langar'
                                    ? require('../../assets/clock.png')
                                    : require('../../assets/quantity_yellow.png')
                                }
                                style={styles.clock}
                              />
                              <Text style={styles.text_style}>
                                {value.type == 'langar'
                                  ? value.time + ' ' + 'days'
                                  : value.quantity + ' ' + 'items'}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                      <View
                        style={[styles.column, styles.column_between_spaceing]}>
                        {index == 0 ? (
                          <>
                            {value.isLiked == 'none' ? (
                              <View
                                style={[
                                  styles.status_container,
                                  value.status == 'pending'
                                    ? styles.pending_style
                                    : value.status == 'confirmed'
                                    ? styles.confirmed_style
                                    : value.status == 'delivered'
                                    ? styles.delivered_style
                                    : styles.rejected_style,
                                ]}>
                                <Text style={styles.status_style}>
                                  {value.status}
                                </Text>
                              </View>
                            ) : (
                              // <>
                              //   {value.isLiked == 'none' ? (
                              //     <View />
                              //   ) : (
                              <Image
                                resizeMode="stretch"
                                source={
                                  value.isLiked == 'like'
                                    ? require('../../assets/like.png')
                                    : require('../../assets/dislike.png')
                                }
                                style={[
                                  styles.like_dislike_icon,
                                  {alignSelf: 'flex-end'},
                                ]}
                              />
                              //   )}
                              // </>
                            )}
                          </>
                        ) : (
                          <View />
                        )}
                        {value.type == 'langar' ? (
                          <View />
                        ) : value.price == 0 ? (
                          <Text style={styles.free_text}>Free</Text>
                        ) : (
                          <Text style={styles.price_text}>
                            Rs {value.price}
                          </Text>
                        )}
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          ) : (
            <View style={[styles.centered_text, {height: '100%'}]}>
              <Text>No food found</Text>
            </View>
          )}
        </View>
      </>
    );
  };
  onBack = async () => {
    if (this.state.from == 'notification') {
      await this.props.navigation.navigate('Notifications');
    }
    if (this.state.from == 'profile') {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'BottomTab'})],
        }),
      );
      await this.props.navigation.navigate('tab5');
    }
    if (
      this.state.from == '' ||
      this.state.from == undefined ||
      this.state.from == null
    ) {
      await this.setState({isVisible: !this.state.isVisible});
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'BottomTab'})],
        }),
      );
      await this.props.navigation.navigate('tab1');
    }
    await this.setState({from: ''});
  };
  render() {
    const {
      container,
      spacing,
      column,
      between_spacing,
      centered_text,
      skipped_content,
    } = styles;
    return (
      <>
        <Header title={'My food'} onBack={this.onBack} />
        <View style={[container, column, between_spacing, spacing]}>
          <View>
            {!this.state.isSkipped ? (
              <Accordion
                style={{border: 'none'}}
                dataArray={this.state.dataArray}
                animation={true}
                expanded={true}
                renderHeader={this.renderHeader}
                renderContent={this.renderContent}
              />
            ) : (
              <View style={[skipped_content, centered_text]}>
                <Text>You have to login first to access this page</Text>
              </View>
            )}
            {this.state.isDialogVisible ? (
              <LikeDislikeFood
                visible={this.state.isDialogVisible}
                closeDialog={this.closeDialog}
                foodId={this.state.foodId}
                sellerId={this.state.sellerId}
              />
            ) : (
              <View />
            )}
          </View>
          <Loader isVisibleLoading={this.state.isVisibleLoading} />
        </View>
      </>
    );
  }
}
