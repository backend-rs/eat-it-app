import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  container: {
    width: '100%',
    height: '100%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  search_container: {
    width: wp(83.2),
    height: hp(5.6),
    borderWidth: 1,
    borderColor: colors.primaryColor,
    borderRadius: 25,
    backgroundColor: '#ECECEC',
  },
  list_height: {
    height: hp(86),
  },
  top_container: {
    width: wp(96),
    marginBottom: hp(1.6),
  },
  price_text: {
    fontSize: 16,
    fontWeight: fonts.FONT_BOLD,
    color: colors.primaryColor,
  },
  address_text: {
    fontSize: 12,
    color: colors.greyText,
  },
  text_style: {
    color: colors.greyText,
    fontSize: fonts.FONT_NORMAL,
  },
  column_between_spacing: {
    justifyContent: 'space-between',
  },
  clock: {
    height: hp(1.4),
    width: hp(1.4),
    marginRight: hp(1),
  },
  row_center_align: {
    alignItems: 'center',
  },
  product_heading: {
    fontSize: fonts.FONT_TEXT,
    fontWeight: fonts.FONT_BOLD,
    textTransform: 'capitalize',
  },
  list_image_continer: {
    height: hp(9.2),
    width: hp(8.8),
    borderWidth: 1.4,
    borderRadius: 5,
    borderColor: colors.primaryColor,
    marginRight: wp(2.4),
  },
  list_image: {
    height: '100%',
    width: '100%',
    borderRadius: 4,
  },
  inner_list_spacing: {
    marginTop: hp(0.2),
    marginBottom: hp(3),
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  around_spacing: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  centered_text: {justifyContent: 'center', alignItems: 'center'},
  search_input: {
    width: wp(62),
    padding: 0,
  },
  icons: {
    width: wp(8.6),
    height: hp(8.6),
  },
  free_text: {
    fontSize: 16,
    fontWeight: fonts.FONT_BOLD,
    color: 'green',
  },
  logo_icons: {
    height: hp(1.4),
    width: hp(1.4),
    marginRight: hp(1),
  },
  search_icon: {
    width: wp(6),
    height: hp(6),
  },
};
export default styles;
