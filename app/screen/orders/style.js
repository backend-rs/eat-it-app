import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  container: {
    width: '100%',
    height: '92%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  list_height: {
    height: hp(82),
  },
  top_container: {
    width: wp(96),
    marginBottom: hp(1.6),
  },
  price_text: {
    fontSize: 16,
    fontWeight: fonts.FONT_BOLD,
    color: colors.primaryColor,
  },
  address_text: {
    color: colors.greyText,
  },
  text_style: {
    color: colors.greyText,
    fontSize: fonts.FONT_NORMAL,
  },
  column_between_spacing: {
    justifyContent: 'space-between',
  },
  clock: {
    height: hp(1.4),
    width: hp(1.4),
    marginRight: hp(1),
  },
  row_center_align: {
    alignItems: 'center',
  },
  product_heading: {
    fontSize: fonts.FONT_TEXT,
    fontWeight: fonts.FONT_BOLD,
    textTransform: 'capitalize',
  },
  list_image_continer: {
    height: hp(9.2),
    width: hp(8.8),
    borderWidth: 1.4,
    borderRadius: 5,
    borderColor: colors.primaryColor,
    marginRight: wp(2.4),
  },
  list_image: {
    height: '100%',
    width: '100%',
    borderRadius: 4,
  },
  inner_list_spacing: {
    marginTop: hp(0.2),
    marginBottom: hp(3),
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  centered_text: {justifyContent: 'center', alignItems: 'center'},
  spacing: {
    marginTop: hp(2.4),
    // marginBottom: hp(3.8),
  },
  end_align: {
    alignItems: 'flex-end',
  },
  free_text: {
    fontSize: 16,
    fontWeight: fonts.FONT_BOLD,
    color: 'green',
  },
  status_container: {
    paddingVertical: hp(0.2),
    paddingHorizontal: hp(0.8),
    borderRadius: 3,
  },
  pending_style: {backgroundColor: 'green'},
  confirmed_style: {backgroundColor: '#FED704'},
  delivered_style: {backgroundColor: colors.primaryColor},
  rejected_style: {backgroundColor: 'red'},
  status_style: {
    textTransform: 'capitalize',
    color: 'white',
    fontSize: fonts.FONT_NORMAL,
  },
  filter_container: {
    marginBottom: hp(2),
    paddingHorizontal: wp(2),
    width: wp(86),
    alignSelf: 'center',
  },
  filters: {
    height: hp(5),
    width: wp(24),
    borderRadius: 30,
    padding: 12,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: wp(0.7),
  },
  filter_text: {
    fontSize: 14,
  },
  selected_color: {
    backgroundColor: colors.gradientFirstColor,
  },
  unselected_color: {
    backgroundColor: colors.unselectedFilter,
  },
  logo_icons: {
    height: hp(1.4),
    width: hp(1.4),
    marginRight: hp(1),
  },
};
export default styles;
