import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  scroll_style: {flex: 1, flexDirection: 'column', justifyContent: 'center'},
  container: {
    width: '100%',
    height: '100%',
  },
  inner_container: {
    width: wp(96),
    alignSelf: 'center',
  },
  arrow: {
    height: hp(4.5),
    width: hp(4.5),
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  around_spacing: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  inner_container: {
    width: wp(96),
    alignSelf: 'center',
  },
  arrow: {
    height: hp(4.5),
    width: hp(4.5),
  },
  heading_text: {
    fontSize: fonts.FONT_HEADING,
    fontWeight: fonts.FONT_BOLD,
  },
  spacing: {
    marginTop: hp(2.4),
    marginBottom: hp(3.8),
  },
  fields_container: {
    width: wp(92),
    alignSelf: 'center',
  },
  indicatorStyle: {backgroundColor: colors.primaryColor, height: 2.5},
  tabBarTextStyle: {fontSize: 15, fontWeight: 'bold', textAlign: 'center'},
  field_label: {
    fontSize: fonts.FONT_TEXT,
  },
  fields_between_spacing: {
    marginBottom: hp(2.6),
  },
  picker_style: {
    width: wp(38),
    height: hp(3.8),
    borderRadius: 20,
    fontSize: 10,
    paddingHorizontal: wp(3.6),
    backgroundColor: colors.unselectedFilter,
  },
  picker_label: {
    fontSize: 13,
  },
  date_picker_text: {color: 'black', fontSize: 13, padding: 0, margin: 0},
  placeholder_text: {color: 'grey', fontSize: 13, padding: 0, margin: 0},
  label_heading: {color: 'grey', fontSize: 15},
  input_box_style: {
    width: wp(28),
    height: hp(6),
    fontSize: 13,
    marginLeft: wp(-1),
  },
  extra_bottom_spacing: {marginBottom: hp(1.3)},
  extra_top_spacing: {marginTop: hp(1.3)},
  price_input_box_style: {
    width: wp(23.8),
    height: hp(6),
    fontSize: 13,
  },
  disabled_color: {
    backgroundColor: '#BFBFC1',
  },
  ckeckbox: {
    marginRight: wp(3),
    borderRadius: 4,
    height: hp(2.6),
    width: hp(2.6),
  },
  checkbox_container: {
    alignItems: 'center',
    width: wp(39.4),
  },
  fields: {
    borderBottomWidth: 1.4,
    borderBottomColor: colors.fieldsColor,
    marginVertical: hp(0.6),
    paddingVertical: hp(0.6),
  },
  description_fields: {
    height: hp(12),
    borderWidth: 1.4,
    borderColor: colors.fieldsColor,
    // marginVertical: hp(0.3),
    // paddingVertical: hp(0.2),
  },
  description_input_box: {
    width: wp(80),
    color: 'black',
  },
  field_icons: {
    height: hp(2.4),
    width: hp(2.4),
    marginRight: wp(2.8),
    padding: 0,
  },
  field_map_icon: {
    height: hp(3),
    width: hp(3),
    padding: 0,
  },
  picker_icons: {
    height: hp(1.8),
    width: hp(1.8),
  },
  address_placeholder: {
    color: '#969696',
  },
  input_box: {
    padding: 0,
    height: hp(2.8),
    width: wp(80),
    color: 'black',
  },

  menu_position: {
    marginTop: hp(5),
  },
  menu_background: {
    backgroundColor: '#ECECEC',
  },
  menu_list_title: {
    fontSize: 13,
    fontWeight: fonts.FONT_BOLD,
    color: colors.greyText,
  },
  list_item_height: {
    height: hp(4),
    paddingHorizontal: wp(0),
    width: wp(38),
  },
  checkbox_list: {
    height: hp(4),
    paddingHorizontal: wp(0),
  },
  bottom_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    width: wp(86),
    marginBottom: hp(3),
  },
  forward_container: {
    height: hp(7.8),
    width: hp(7.8),
    borderRadius: hp(7.8) / 2,
    backgroundColor: '#FFBA09',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginTop: hp(6),
    elevation: 6,
  },
  vertical_spacing: {
    marginVertical: hp(4),
  },
  button_container: {
    height: hp(5.4),
    width: wp(32),
    borderRadius: 20,
    marginTop: hp(2),
    alignSelf: 'flex-end',
    elevation: 4,
  },
  centered_text: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    color: 'white',
  },
};
export default styles;
