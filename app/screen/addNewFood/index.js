import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import styles from './style';
import {TabView, TabBar} from 'react-native-tab-view';
import * as Service from '../../api/services';
import * as utility from '../../utility/index';
import * as Url from '../../constants/urls';
import * as colors from '../../constants/colors';
import {Provider} from 'react-native-paper';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import {NavigationActions, StackActions} from 'react-navigation';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';
import Loader from '../../components/Loader';
import Header from '../../components/header';
import MenuList from '../../components/menuList';

export default class addFood extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      cuisions: [],
      cuision: '',
      visibleCuisions: false,
      isChecked: [],
      selectedLists: [],
      visibleCategory: false,
      category: 'Veg',
      categories: [
        {
          name: 'Veg',
        },
        {
          name: 'Non-veg',
        },
      ],
      visibleLocation: false,
      location: 'Pro',
      locations: [
        {
          name: 'Pro',
        },
        {
          name: 'Other',
        },
      ],
      dishAddress: '',
      visibleFrom: false,
      from: 'Restaurant',
      cookedFrom: [
        {
          name: 'Restaurant',
        },
        {
          name: 'Home made',
        },
      ],
      isLangar: false,
      homedelivery: 'Yes',
      visibleHomeDelivery: false,
      homeDelValues: [
        {
          name: 'Yes',
        },
        {
          name: 'No',
        },
      ],
      noHomeDelivery: false,
      deliveryPrice: 0,
      fromTime: new Date(),

      toPickupTime: '',
      isToTimePickerVisible: false,
      toPickupDateTime: Date(),

      isFromTimePickerVisible: false,
      fromPickupTime: '',
      fromPickupDateTime: Date(),

      dishName: '',
      description: '',
      price: 0,

      isFirstScreen: true,
      isSecondScreen: false,
      isThirdScreen: false,

      visibleAvailTime: false,
      availTime: 1,
      days: [
        {name: 1},
        {name: 2},
        {name: 3},
        {name: 4},
        {name: 5},
        {name: 6},
        {name: 7},
      ],
      quantity: 0,

      routes: [
        {key: '1', title: 'LANGAR'},
        {key: '2', title: 'FREE FOOD'},
        {key: '3', title: 'PAID FOOD'},
      ],
      userToken: '',
      userId: '',
      isVisibleLoading: false,
      images: [],
      photos: [],
      totalPrice: 0,
    };
    this.handleConfirm = this.handleConfirm.bind(this);
    this.onRefersh = this.onRefersh.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onOpen = this.onOpen.bind(this);
    this.onClose = this.onClose.bind(this);
  }
  componentDidMount = async () => {
    const token = await utility.getToken('token');
    const userId = await utility.getItem('userId');
    await this.setState({userToken: token, userId: userId});
    await this.getUser();
    await this.getCuisions();
    let initialCheck = this.state.cuisions.map(() => false);
    await this.setState({isChecked: initialCheck});
    if (this.props.navigation.state.params) {
      if (
        this.props.navigation.state.params.photos &&
        this.props.navigation.state.params.photos.length != 0
      ) {
        let photos = this.props.navigation.state.params.photos;
        await this.setState({photos: photos});
      }
    }
  };
  onChecked = async (index, item) => {
    let {isChecked, selectedLists} = this.state;
    isChecked[index] = !isChecked[index];
    this.setState({isChecked: isChecked});
    if (isChecked[index] == true) {
      if (selectedLists.length < 3) {
        selectedLists.push({
          id: item.id,
        });
        this.setState({cuision: item.name});
      } else {
        isChecked[index] = !isChecked[index];
        this.setState({isChecked: isChecked});
        utility.alert('You can add only three cuisions');
      }
    } else {
      selectedLists.pop({
        id: item.id,
      });
      if (selectedLists.length == 0) {
        this.setState({cuision: ''});
      }
    }
  };
  getCuisions = async () => {
    this.setState({cuisions: []});
    try {
      let response = Service.getDataApi(Url.GET_CUISIONS, '');
      response
        .then(res => {
          if (res.data) {
            if (res.data.length != 0) {
              let tempCuisions = [];
              for (let i = 0; i < res.data.length; i++) {
                tempCuisions.push({
                  id: res.data[i]._id,
                  name:
                    res.data[i].name.slice(0, 1).toUpperCase() +
                    res.data[i].name.slice(1, res.data[i].name.length),
                });
              }
              this.setState({
                cuisions: tempCuisions,
              });
            }
          } else {
            console.log('if no data in response:', res.error);
            utility.alert(res.error);
          }
        })
        .catch(error => {
          console.log('api problem:', error.error);
          utility.alert(error.error);
        });
    } catch (err) {
      console.log('another problem:', err);
      utility.alert(err);
    }
  };
  getUser = async () => {
    await this.setState({isVisibleLoading: true});
    try {
      let response = Service.getDataApi(
        `users/${this.state.userId}`,
        this.state.userToken,
      );
      response
        .then(res => {
          if (res.data) {
            if (res.data.address) {
              this.setState({
                address: res.data.address,
                dishAddress: res.data.address.line,
                isVisibleLoading: false,
              });
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onUploadImage = async file => {
    await this.setState({isVisibleLoading: true});

    var formData = new FormData();
    let fileData = {
      uri: file.uri,
      name: file.fileName,
      type: file.type,
    };
    formData.append('file', fileData);
    const headers = {
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json',
    };
    try {
      let response = Service.uploadImageApi(
        Url.UPLOAD_IMAGE,
        formData,
        headers,
      );
      response
        .then(res => {
          if (res.data) {
            if (res.data != null) {
              if (res.data.image != null) {
                var joined = this.state.images.concat({
                  url: res.data.image.url,
                  resize_url: res.data.image.resize_url,
                  thumbnail: res.data.image.thumbnail,
                  resize_thumbnail: res.data.image.resize_thumbnail,
                });
                this.setState({images: joined});
              }
              if (this.state.images.length == this.state.photos.length) {
                this.onAddFood();
              }
            }
            this.setState({isVisibleLoading: false});
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onAddPhotos = async () => {
    if (this.state.photos && this.state.photos != 0) {
      for (let file of this.state.photos) {
        if (file) {
          await this.onUploadImage(file);
        }
      }
    }
  };
  onAddFood = async () => {
    let type;
    let foodCooked;
    let price;
    let body;

    if (this.state.category == 'Non-veg') {
      type = 'nonVeg';
    } else if (this.state.category == 'Langar') {
      type = 'langar';
    } else {
      type = 'veg';
    }
    if (this.state.from == 'Restaurant') {
      foodCooked = 'restaurant';
    } else {
      foodCooked = 'homemade';
    }
    if (this.state.isLangar) {
      price = 0;
      body = {
        type: type,
        availabilityTime: this.state.availTime,
        pickupTime: {
          from: this.state.fromPickupTime,
          to: this.state.toPickupTime,
        },
        name: this.state.dishName,
        address: this.state.address,
        description: this.state.description,
        images: this.state.images,
        price: price,
      };
    } else {
      price = this.state.price;
      body = {
        name: this.state.dishName,
        type: type,
        homeDelivery: this.state.homedelivery.toLowerCase(),
        homeDeliveryPrice: this.state.deliveryPrice,
        quantity: this.state.quantity,
        availabilityTime: this.state.availTime,
        pickupTime: {
          from: this.state.fromPickupTime,
          to: this.state.toPickupTime,
        },
        foodCooked: foodCooked,
        price: price,
        address: this.state.address,
        description: this.state.description,
        cuisine: this.state.selectedLists,
        images: this.state.images,
      };
    }
    this.setState({isVisibleLoading: true});
    try {
      let response = Service.postDataApi(
        Url.ADD_FOOD,
        body,
        this.state.userToken,
      );
      response
        .then(res => {
          if (res.data) {
            if (res.isSuccess == true) {
              utility.alert('Added successfully');
              this.setState({
                cuision: '',
                category: 'Veg',
                from: 'Restaurant',
                cookingTime: '',
                quantity: 0,
                availTime: 1,
                toPickupDateTime: Date(),
                fromPickupDateTime: Date(),
                homedelivery: 'Yes',
                deliveryPrice: 0,
                price: 0,
                isVisibleLoading: false,
              });
              this.props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({routeName: 'BottomTab'}),
                  ],
                }),
              );
              this.props.navigation.navigate('tab1');
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  closeCuisionMenu = async () => {
    await this.setState({visibleCuisions: false});
  };
  openCuisionMenu = async () => {
    await this.setState({visibleCuisions: true});
  };
  onHomeDeliveryChange = async value => {
    if (value.name == 'No') {
      await this.setState({noHomeDelivery: true});
    } else {
      await this.setState({noHomeDelivery: false});
    }
    await this.setState({homedelivery: value.name, visibleHomeDelivery: false});
  };
  showTimePicker = async from => {
    if (from == 'fromPickupTime') {
      await this.setState({isFromTimePickerVisible: true});
    }
    if (from == 'toPickupTime') {
      await this.setState({isToTimePickerVisible: true});
    }
  };
  hideTimePicker = async from => {
    if (from == 'fromPickupTime') {
      await this.setState({isFromTimePickerVisible: false});
    }
    if (from == 'toPickupTime') {
      await this.setState({isToTimePickerVisible: false});
    }
  };
  handleConfirm = async (date, from) => {
    let pastTime;
    let isAfter;
    let time = moment(date).format('hh:mm a');

    if (from == 'fromPickupTime') {
      await this.setState({
        fromPickupTime: time,
        isFromTimePickerVisible: false,
        isToTimePickerVisible: false,
        fromPickupDateTime: date,
      });
    }
    if (from == 'toPickupTime') {
      if (this.state.fromPickupTime == '') {
        utility.alert('Select first From Pickup time');
        return;
      }
      pastTime = moment(this.state.fromPickupTime, 'hh:mm a');
      isAfter = moment(time, 'hh:mm a').isAfter(pastTime);
      if (!isAfter) {
        utility.alert('To Pickup time should be after that from pickup time');
        return;
      } else {
        await this.setState({
          toPickupTime: time,
          toPickupDateTime: date,
          isFromTimePickerVisible: false,
          isToTimePickerVisible: false,
        });
      }
    }
  };
  onCategoryChange = async value => {
    await this.setState({category: value.name, visibleCategory: false});
  };
  onLocationChange = async value => {
    if (value.name == 'Other') {
      await this.onLocation();
    }
    await this.setState({location: value.name, visibleLocation: false});
  };
  onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    this.setState({fromTime: currentDate});
  };
  onOpen = async id => {
    if (id == 1) {
      await this.setState({visibleCategory: true});
    }
    if (id == 2) {
      await this.setState({visibleFrom: true});
    }
    if (id == 3) {
      await this.setState({visibleAvailTime: true});
    }
    if (id == 4) {
      await this.setState({visibleLocation: true});
    }
    if (id == 5) {
      await this.setState({visibleHomeDelivery: true});
    }
    if (id == 6) {
      await this.setState({visibleCuisions: true});
    }
  };
  onClose = async id => {
    if (id == 1) {
      await this.setState({visibleCategory: false});
    }
    if (id == 2) {
      await this.setState({visibleFrom: false});
    }
    if (id == 3) {
      await this.setState({visibleAvailTime: false});
    }
    if (id == 4) {
      await this.setState({visibleLocation: false});
    }
    if (id == 5) {
      await this.setState({visibleHomeDelivery: false});
    }
    if (id == 6) {
      await this.setState({visibleCuisions: false});
    }
  };
  onSelect = async (value, id, index) => {
    if (id == 1) {
      await this.onCategoryChange(value);
    }
    if (id == 2) {
      await this.setState({from: value.name, visibleFrom: false});
    }
    if (id == 3) {
      await this.setState({availTime: value.name, visibleAvailTime: false});
    }
    if (id == 4) {
      await this.onLocationChange(value);
    }
    if (id == 5) {
      this.onHomeDeliveryChange(value);
    }
    if (id == 6) {
      this.onChecked(index, value);
    }
  };
  renderFirstSlideUniqueFields = selectedIndex => {
    return this.state.index == selectedIndex ? (
      <>
        {this.state.index != 0 ? (
          <>
            <View
              style={[
                styles.row,
                styles.between_spacing,
                styles.fields_between_spacing,
              ]}>
              <Text style={styles.field_label}>Category</Text>
              <MenuList
                visible={this.state.visibleCategory}
                close={this.onClose}
                open={this.onOpen}
                list={this.state.categories}
                selectedItem={this.state.category}
                onSelect={this.onSelect}
                id={1}
              />
            </View>
            <View
              style={[
                styles.row,
                styles.between_spacing,
                styles.fields_between_spacing,
              ]}>
              <Text style={styles.field_label}>Prepared at</Text>
              <MenuList
                visible={this.state.visibleFrom}
                close={this.onClose}
                open={this.onOpen}
                list={this.state.cookedFrom}
                selectedItem={this.state.from}
                onSelect={this.onSelect}
                id={2}
              />
            </View>
            <View
              style={[
                styles.row,
                styles.between_spacing,
                styles.fields_between_spacing,
              ]}>
              <Text style={styles.field_label}>Pickup time</Text>
              <View style={styles.column}>
                <TouchableOpacity
                  onPress={() => this.showTimePicker('fromPickupTime')}>
                  <View
                    style={[
                      styles.row,
                      styles.picker_style,
                      styles.between_spacing,
                      styles.extra_bottom_spacing,
                    ]}>
                    <DateTimePickerModal
                      isVisible={this.state.isFromTimePickerVisible}
                      mode="time"
                      display="spinner"
                      placeHolderText="From Time"
                      is24Hour={false}
                      onConfirm={date =>
                        this.handleConfirm(date, 'fromPickupTime')
                      }
                      onCancel={() => this.hideTimePicker('fromPickupTime')}
                    />
                    {this.state.fromPickupTime == '' ? (
                      <Text style={styles.placeholder_text}>From time</Text>
                    ) : (
                      <Text style={styles.picker_label}>
                        {this.state.fromPickupTime}
                      </Text>
                    )}
                    <Image
                      source={require('../../assets/clock_black.png')}
                      style={styles.picker_icons}
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.showTimePicker('toPickupTime')}>
                  <View
                    style={[
                      styles.row,
                      styles.picker_style,
                      styles.between_spacing,
                      styles.extra_top_spacing,
                    ]}>
                    <DateTimePickerModal
                      isVisible={this.state.isToTimePickerVisible}
                      mode="time"
                      display="spinner"
                      placeHolderText="To Time"
                      is24Hour={false}
                      onConfirm={date =>
                        this.handleConfirm(date, 'toPickupTime')
                      }
                      onCancel={() => this.hideTimePicker('toPickupTime')}
                    />

                    {this.state.toPickupTime == '' ? (
                      <Text style={styles.placeholder_text}>To time</Text>
                    ) : (
                      <Text style={styles.picker_label}>
                        {this.state.toPickupTime}
                      </Text>
                    )}
                    <Image
                      source={require('../../assets/clock_black.png')}
                      style={styles.picker_icons}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={[
                styles.row,
                styles.between_spacing,
                styles.fields_between_spacing,
              ]}>
              <Text style={styles.field_label}>Cusinies</Text>
              <MenuList
                visible={this.state.visibleCuisions}
                close={this.onClose}
                open={this.onOpen}
                list={this.state.cuisions}
                selectedItem={this.state.cuision}
                onSelect={this.onSelect}
                isChecked={this.state.isChecked}
                id={6}
              />
            </View>
          </>
        ) : (
          <View />
        )}
        <View
          style={[styles.row, styles.fields, styles.fields_between_spacing]}>
          <Image
            source={require('../../assets/dish.png')}
            style={styles.field_icons}
          />
          <TextInput
            placeholder="Dish name"
            style={styles.input_box}
            onChangeText={dishName => this.setState({dishName})}
            value={this.state.dishName}
          />
        </View>
        <View>
          <Text style={styles.label_heading}>Description</Text>
          <View style={[styles.column, styles.description_fields]}>
            <TextInput
              multiline={true}
              numberOfLines={10}
              placeholder=""
              style={styles.description_input_box}
              onChangeText={description => this.setState({description})}
              value={this.state.description}
            />
          </View>
        </View>
      </>
    ) : (
      <View />
    );
  };
  renderSecondSlideUniqueFields = selectedIndex => {
    return this.state.index == selectedIndex ? (
      <>
        {this.state.index != 0 ? (
          <View
            style={[
              styles.row,
              styles.between_spacing,
              styles.fields_between_spacing,
            ]}>
            <Text style={styles.field_label}>Quantity</Text>
            <TouchableOpacity>
              <View
                style={[
                  styles.row,
                  styles.picker_style,
                  styles.between_spacing,
                ]}>
                <Text style={styles.picker_label}>Items</Text>
                <TextInput
                  placeholder="0"
                  keyboardType={'numeric'}
                  style={[styles.price_input_box_style, {width: wp(18)}]}
                  onChangeText={quantity => this.setState({quantity})}
                  value={this.state.quantity}
                />
                <Image
                  source={require('../../assets/quantity_black.png')}
                  style={styles.picker_icons}
                />
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <View />
        )}
        <View
          style={[
            styles.row,
            styles.between_spacing,
            styles.fields_between_spacing,
          ]}>
          <Text style={styles.field_label}>Availability time</Text>
          <MenuList
            visible={this.state.visibleAvailTime}
            close={this.onClose}
            open={this.onOpen}
            list={this.state.days}
            selectedItem={this.state.availTime}
            onSelect={this.onSelect}
            id={3}
          />
        </View>
      </>
    ) : (
      <View />
    );
  };
  renderThirdSlideUniqueFields = selectedIndex => {
    return this.state.index == selectedIndex ? (
      <>
        <View
          style={[
            styles.row,
            styles.between_spacing,
            styles.fields_between_spacing,
          ]}>
          <Text style={styles.field_label}>Location</Text>
          <MenuList
            visible={this.state.visibleLocation}
            close={this.onClose}
            open={this.onOpen}
            list={this.state.locations}
            selectedItem={this.state.location}
            onSelect={this.onSelect}
            id={4}
          />
        </View>
        <View style={[styles.row, styles.fields_between_spacing]}>
          <Text style={styles.field_label}>Address</Text>
          <Text style={[styles.field_label, {marginHorizontal: hp(6)}]}>
            {this.state.dishAddress}
          </Text>
        </View>
      </>
    ) : (
      <View />
    );
  };
  onAddPrice = async price => {
    let tempPrice = 0;
    if (price == null || price == undefined || price == '' || price == 0) {
      tempPrice = 0;
    } else {
      tempPrice = price;
    }
    await this.setState({price: price});
    let totalPrice =
      JSON.parse(tempPrice) + JSON.parse(this.state.deliveryPrice);
    await this.setState({totalPrice: totalPrice});
  };
  onAddDeliveryCharges = async deliveryprice => {
    let tempdeliveryPrice = 0;
    if (
      deliveryprice == null ||
      deliveryprice == undefined ||
      deliveryprice == '' ||
      deliveryprice == 0
    ) {
      tempdeliveryPrice = 0;
    } else {
      tempdeliveryPrice = deliveryprice;
    }
    if (this.state.homedelivery == 'No') {
      if (tempdeliveryPrice != 0) {
        utility.alert('Home delivery should be enabled for delivery price');
        return;
      }
    }
    await this.setState({deliveryPrice: tempdeliveryPrice});

    let totalPrice =
      JSON.parse(tempdeliveryPrice) + JSON.parse(this.state.price);
    await this.setState({totalPrice: totalPrice});
  };
  _renderScene = ({route}) => {
    const {
      fields_container,
      vertical_spacing,
      bottom_container,
      forward_container,
      button_container,
      centered_text,
      button_text,
      arrow,
    } = styles;
    switch (route.key) {
      case '1':
        return (
          <View style={vertical_spacing}>
            <View style={fields_container}>
              {this.state.isFirstScreen
                ? this.renderFirstSlideUniqueFields(0)
                : this.state.isSecondScreen
                ? this.renderSecondSlideUniqueFields(0)
                : this.renderThirdSlideUniqueFields(0)}
            </View>
            <View style={bottom_container}>
              <Text />
              {!this.state.isThirdScreen ? (
                <TouchableOpacity
                  onPress={this.onNext}
                  style={forward_container}>
                  <Image
                    resizeMode="contain"
                    source={require('../../assets/next_button_arrow.png')}
                    style={arrow}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity activeOpacity={0.6} onPress={this.onNext}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    colors={[
                      colors.gradientFirstColor,
                      colors.gradientSecondColor,
                    ]}
                    style={[button_container, centered_text]}>
                    <Text style={button_text}>Done</Text>
                  </LinearGradient>
                </TouchableOpacity>
              )}
            </View>
          </View>
        );
      case '2':
        return (
          <KeyboardAvoidingView
            style={styles.scroll_style}
            behavior="padding"
            enabled
            keyboardVerticalOffset={50}>
            <ScrollView contentContainerStyle={{flexGrow: 1}}>
              <View style={vertical_spacing}>
                <View style={fields_container}>
                  {this.state.isFirstScreen
                    ? this.renderFirstSlideUniqueFields(1)
                    : this.state.isSecondScreen
                    ? this.renderSecondSlideUniqueFields(1)
                    : this.renderThirdSlideUniqueFields(1)}
                </View>
                <View style={bottom_container}>
                  <Text />
                  {!this.state.isThirdScreen ? (
                    <TouchableOpacity
                      style={forward_container}
                      onPress={this.onNext}>
                      <Image
                        resizeMode="contain"
                        source={require('../../assets/next_button_arrow.png')}
                        style={arrow}
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity activeOpacity={0.6} onPress={this.onNext}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        colors={[
                          colors.gradientFirstColor,
                          colors.gradientSecondColor,
                        ]}
                        style={[button_container, centered_text]}>
                        <Text style={button_text}>Done</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        );
      case '3':
        return (
          <KeyboardAvoidingView
            style={styles.scroll_style}
            behavior="padding"
            enabled
            keyboardVerticalOffset={50}>
            <ScrollView contentContainerStyle={{flexGrow: 1}}>
              <View style={vertical_spacing}>
                <View style={fields_container}>
                  {this.state.isFirstScreen ? (
                    this.renderFirstSlideUniqueFields(2)
                  ) : this.state.isSecondScreen ? (
                    <>
                      {this.renderSecondSlideUniqueFields(2)}
                      <View
                        style={[
                          styles.row,
                          styles.between_spacing,
                          styles.fields_between_spacing,
                        ]}>
                        <Text style={styles.field_label}>Rate</Text>
                        <TouchableOpacity>
                          <View
                            style={[
                              styles.row,
                              styles.picker_style,
                              styles.between_spacing,
                            ]}>
                            <Text style={styles.picker_label}>Rs</Text>
                            <TextInput
                              placeholder="0"
                              style={styles.price_input_box_style}
                              onChangeText={price => this.onAddPrice(price)}
                              keyboardType={'numeric'}
                              value={this.state.price}
                            />
                            <Image
                              source={require('../../assets/price_tag.png')}
                              style={styles.picker_icons}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View
                        style={[
                          styles.row,
                          styles.between_spacing,
                          styles.fields_between_spacing,
                        ]}>
                        <Text style={styles.field_label}>Delivery</Text>
                        <MenuList
                          visible={this.state.visibleHomeDelivery}
                          close={this.onClose}
                          open={this.onOpen}
                          list={this.state.homeDelValues}
                          selectedItem={this.state.homedelivery}
                          onSelect={this.onSelect}
                          id={5}
                        />
                      </View>
                      <View
                        style={[
                          styles.row,
                          styles.between_spacing,
                          styles.fields_between_spacing,
                        ]}>
                        <Text style={styles.field_label}>Delivery charges</Text>
                        <TouchableOpacity>
                          <View
                            style={[
                              styles.row,
                              styles.picker_style,
                              styles.between_spacing,
                            ]}>
                            <Text style={styles.picker_label}>Rs</Text>
                            <TextInput
                              placeholder="0"
                              keyboardType={'numeric'}
                              style={styles.price_input_box_style}
                              onChangeText={deliveryPrice =>
                                this.onAddDeliveryCharges(deliveryPrice)
                              }
                              value={this.state.deliveryPrice}
                            />
                            <Image
                              source={require('../../assets/price_tag.png')}
                              style={styles.picker_icons}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View
                        style={[
                          styles.row,
                          styles.between_spacing,
                          styles.fields_between_spacing,
                        ]}>
                        <Text style={styles.field_label}>
                          Total amount to pay
                        </Text>
                        <View style={{width: wp(24)}}>
                          <Text style={[styles.field_label, {fontSize: 16}]}>
                            Rs {this.state.totalPrice}
                          </Text>
                        </View>
                      </View>
                    </>
                  ) : (
                    this.renderThirdSlideUniqueFields(2)
                  )}
                </View>
                <View style={bottom_container}>
                  <Text />
                  {!this.state.isThirdScreen ? (
                    <TouchableOpacity
                      style={forward_container}
                      onPress={this.onNext}>
                      <Image
                        resizeMode="contain"
                        source={require('../../assets/next_button_arrow.png')}
                        style={arrow}
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity activeOpacity={0.6} onPress={this.onNext}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        colors={[
                          colors.gradientFirstColor,
                          colors.gradientSecondColor,
                        ]}
                        style={[button_container, centered_text]}>
                        <Text style={button_text}>Done</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        );
      default:
        return null;
    }
  };
  renderTabBar(props) {
    return (
      <TabBar
        {...props}
        style={{backgroundColor: '#F2F2F2'}}
        indicatorStyle={styles.indicatorStyle}
        renderLabel={({route}) => (
          <View>
            <Text
              style={[
                styles.tabBarTextStyle,
                {
                  color:
                    route.key ===
                    props.navigationState.routes[props.navigationState.index]
                      .key
                      ? colors.primaryColor
                      : 'black',
                },
              ]}>
              {route.title}
            </Text>
          </View>
        )}
      />
    );
  }
  onNext = async () => {
    if (this.state.index == 0) {
      if (this.state.isFirstScreen) {
        if (
          utility.isFieldEmpty(this.state.dishName && this.state.description)
        ) {
          utility.alert('All fields are required');
          return;
        } else {
          await this.setState({
            isFirstScreen: false,
            isSecondScreen: true,
            isThirdScreen: false,
            category: 'Langar',
            isLangar: true,
          });
          return;
        }
      }
      if (this.state.isSecondScreen) {
        if (utility.isFieldEmpty(this.state.availTime)) {
          utility.alert('All fields are required');
          return;
        } else {
          await this.setState({
            isFirstScreen: false,
            isSecondScreen: false,
            isThirdScreen: true,
          });
          return;
        }
      }
    }
    if (this.state.index == 1 || this.state.index == 2) {
      if (this.state.isFirstScreen) {
        if (
          utility.isFieldEmpty(
            this.state.category &&
              this.state.from &&
              this.state.dishName &&
              this.state.description &&
              this.state.fromPickupTime &&
              this.state.toPickupTime,
          )
        ) {
          utility.alert('All fields are required');
          return;
        } else if (
          this.state.selectedLists &&
          this.state.selectedLists.length == 0
        ) {
          utility.alert('Cuisions are required');
          return;
        } else {
          await this.setState({
            isFirstScreen: false,
            isSecondScreen: true,
            isThirdScreen: false,
            isLangar: false,
          });
          return;
        }
      }
    }
    if (this.state.index == 1) {
      if (this.state.isSecondScreen) {
        if (utility.isFieldEmpty(this.state.quantity && this.state.availTime)) {
          utility.alert('All fields are required');
          return;
        } else {
          await this.setState({
            isFirstScreen: false,
            isSecondScreen: false,
            isThirdScreen: true,
            price: 0,
            deliveryPrice: 0,
          });
          return;
        }
      }
    }
    if (this.state.index == 2) {
      if (this.state.isSecondScreen) {
        if (
          utility.isFieldEmpty(
            this.state.quantity &&
              this.state.availTime &&
              this.state.price &&
              this.state.homedelivery,
          )
        ) {
          utility.alert('All fields are required');
          return;
        } else {
          await this.setState({
            isFirstScreen: false,
            isSecondScreen: false,
            isThirdScreen: true,
          });
          return;
        }
      }
    }
    if (
      this.state.index == 0 ||
      this.state.index == 1 ||
      this.state.index == 2
    ) {
      if (this.state.isThirdScreen) {
        if (utility.isFieldEmpty(this.state.dishAddress)) {
          utility.alert('Location is required');
          return;
        } else {
          await this.onAddPhotos();
        }
      }
    }
  };
  onIndexChange = async index => {
    await this.setState({
      index: index,
      isFirstScreen: true,
      isSecondScreen: false,
      isThirdScreen: false,
      description: '',
      price: 0,
      dishName: '',
      category: 'Veg',
      from: 'Restaurant',
      quantity: 0,
      availTime: 1,
      homedelivery: 'Yes',
      deliveryPrice: 0,
      totalPrice: 0,
      fromPickupTime: '',
      toPickupTime: '',
      cuision: '',
      isChecked: [],
      selectedLists: [],
    });
  };
  onRefersh = async address => {
    await this.setState({address: address, dishAddress: address.line});
  };
  onLocation = async () => {
    await this.props.navigation.push('Map', {
      refersh: this.onRefersh,
      from: 'addFood',
    });
  };
  onBack = async () => {
    this.props.navigation.navigate('AttatchFoodPhotos');
  };
  render() {
    const {container, column} = styles;
    const {index, routes} = this.state;
    return (
      <Provider>
        <Header title={'Add food detail'} onBack={this.onBack} />
        <View style={[container, column]}>
          <TabView
            navigationState={{index, routes}}
            renderScene={this._renderScene}
            onIndexChange={this.onIndexChange}
            renderTabBar={this.renderTabBar}
          />
          <Loader isVisibleLoading={this.state.isVisibleLoading} />
        </View>
      </Provider>
    );
  }
}
