import React, {Component} from 'react';
import {View, Image, Text, TouchableOpacity, ScrollView} from 'react-native';
import styles from './style';
import ShareFood from '../shareFood';
import * as utility from '../../utility/index';
import {NavigationActions, StackActions} from 'react-navigation';
import Header from '../../components/header';
import * as image from '../../components/imageCropper';

export default class attatchFoodPhotos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      isDialogVisible: false,
      files: [],
      images: [],
    };
  }
  componentDidMount = async () => {
    let isSkipped = await utility.getItem('isSkipped');
    console.log('skipped', isSkipped);

    if (isSkipped == true) {
      await this.setState({isDialogVisible: false});
      await utility.showAlert('Please login first.', this.onLogin);
      await this.onBack();
      return;
    } else {
      await this.setState({isDialogVisible: true});
      await this.showDialog();
    }
  };
  onLogin = async () => {
    await this.props.navigation.navigate('Login');
  };
  showDialog = async () => {
    await this.setState({isDialogVisible: true});
  };
  closeDialog = async () => {
    await this.setState({isDialogVisible: false});
  };
  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }
  onImageCrop = async data => {
    let response = await image.onImageCrop(data);
    console.log('imagee cropper responsee::', response);
    if (response != null || response != undefined) {
      var joined = this.state.photos.concat({
        uri: response.uri,
        fileName: data.fileName,
        type: data.type,
      });
      await this.setState({photos: joined});
    } else {
      console.log('cropper error');
    }
  };

  onLaunchCamera = async () => {
    if (this.state.photos && this.state.photos.length < 10) {
      let response = await image.onImagePicker();
      console.log('imagee picker responsee::', response);
      if (response != null || response != undefined) {
        await this.onImageCrop(response);
      } else {
        console.log('picker error');
      }
    } else {
      utility.alert('Unable to add more images');
    }
  };

  onRemove = async item => {
    let index = this.state.photos.indexOf(item);
    this.state.photos.splice(index, 1);
    let joined = this.state.photos;
    await this.setState({photos: joined});
  };
  onBack = async () => {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: 'BottomTab'})],
      }),
    );
    await this.props.navigation.navigate('tab1');
  };
  onNext = async () => {
    if (this.state.photos && this.state.photos != 0) {
      await this.props.navigation.navigate('AddNewFood', {
        photos: this.state.photos,
      });
    } else {
      utility.alert('Select images first');
    }
  };

  render() {
    const {
      container,
      inner_container,
      bottom_container,
      photo_continer,
      spacing,
      centered_text,
      cross_container,
      cross_icon,
      row_list,
      photo_style,
      forward_container,
      row,
      arrow,
      column,
      between_spacing,
      add_style,
    } = styles;
    return (
      <>
        <Header title={'Attach photos'} onBack={this.onBack} />
        <View style={[container, column, between_spacing]}>
          <ScrollView>
            <View>
              {this.state.isDialogVisible ? (
                <ShareFood
                  visible={this.state.isDialogVisible}
                  closeDialog={this.closeDialog}
                  navigation={this.props.navigation}
                />
              ) : (
                <View />
              )}

              <View style={[inner_container, row, row_list, spacing]}>
                {this.state.photos.map(item => {
                  return (
                    <View style={row}>
                      <View style={[photo_continer, centered_text]}>
                        <Image
                          resizeMode="cover"
                          source={{uri: item.uri}}
                          style={photo_style}
                        />
                      </View>
                      <View style={[cross_container, centered_text]}>
                        <TouchableOpacity
                          activeOpacity={0.7}
                          onPress={() => this.onRemove(item)}>
                          <Image
                            resizeMode="cover"
                            source={require('../../assets/cross.png')}
                            style={cross_icon}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  );
                })}

                <TouchableOpacity onPress={this.onLaunchCamera}>
                  <View style={[photo_continer, centered_text]}>
                    <Image
                      resizeMode="cover"
                      source={require('../../assets/add.png')}
                      style={add_style}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
          <View style={bottom_container}>
            <Text />
            {!this.state.isDialogVisible ? (
              <TouchableOpacity onPress={this.onNext} style={forward_container}>
                <Image
                  resizeMode="contain"
                  source={require('../../assets/next_button_arrow.png')}
                  style={arrow}
                />
              </TouchableOpacity>
            ) : (
              <View />
            )}
          </View>
        </View>
      </>
    );
  }
}
