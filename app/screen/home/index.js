import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  BackHandler,
  ScrollView,
} from 'react-native';
import styles from './style';
import {Badge} from 'react-native-elements';
import HandleBack from '../../components/HandleBack';
import * as Service from '../../api/services';
import * as utility from '../../utility/index';
import * as Url from '../../constants/urls';
import * as notification from '../../components/notification';
import Geolocation from '@react-native-community/geolocation';
import {PermissionsAndroid} from 'react-native';
import Loader from '../../components/Loader';
import SocketIOClient from 'socket.io-client';

export default class home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      userId: '',
      userToken: '',
      isSkipped: false,
      pageNo: 1,
      foods: [],
      filtersList: [
        {name: 'For you'},
        {name: 'Near you'},
        {name: 'Free'},
        {name: 'Last search'},
        {name: 'Follow'},
        {name: 'Langar'},
        {name: 'Lunch'},
        {name: 'Breakfast'},
        {name: 'Dinner'},
      ],
      selectedIndex: 0,
      name: '',
      isVisibleLoading: false,
      noDataExist: false,
      isAddressExists: false,
      coordinates: {
        latitude: 0,
        longitude: 0,
      },
      notificationCount: 0,
    };
    {
      global.user !== undefined ? (this.socket = SocketIOClient(Url.URL)) : '';
    }
    {
      global.user !== undefined
        ? this.socket.on('connect', () => {
            console.log('socketio chat connected.', global.user.firstName);
            this.socket.emit(
              'set-user-data',
              global.user.firstName,
              global.user._id,
            );
          })
        : '';
    }

    this.refersh = this._handleNotificationOpen.bind(this);
    this.onGetAddress = this.onGetAddress.bind(this);
  }

  componentDidMount = async () => {
    // console.log('global user', global.user);
    let isAddressExists = false;
    let alertMessage;
    let notificationCount;
    let isSkipped = await utility.getItem('isSkipped');
    const userId = await utility.getItem('userId');
    const userToken = await utility.getToken('token');
    let loginType = await utility.getItem('loginType');
    if (loginType == 'app') {
      alertMessage = 'Please enter location first';
    } else {
      alertMessage = 'Please enter location and phone number first';
    }
    await this.setState({
      isSkipped: isSkipped,
      userId: userId,
      userToken: userToken,
      selectedIndex: 0,
    });
    if (isSkipped == false) {
      isAddressExists = await utility.getItem('isAddressExists');
      notificationCount = await utility.getItem('notificationCount');
      await this.setState({
        isAddressExists: isAddressExists,
        query: `forYou=true&userId=${this.state.userId}`,
        notificationCount: notificationCount,
      });
      if (this.state.isAddressExists) {
        await this.onTokenUpdate();
      } else {
        await utility.showAlertWithCallBack(alertMessage, this.onLocation);
      }
      await this.getFood();
    } else {
      let coordinates = await utility.getItem('coordinates');
      if (coordinates == null || coordinates == undefined) {
        await this.requestCameraPermission();
      } else {
        await this.onUserPinDragEnd(coordinates);
      }
      if (this.props.navigation.state.params) {
        if (
          (this.props.navigation.state.params.from &&
            (this.props.navigation.state.params.from == 'profile' ||
              this.props.navigation.state.params.from == 'orderDetails' ||
              this.props.navigation.state.params.from == 'foodDetails')) ||
          this.props.navigation.state.params.from == 'myFood'
        ) {
          console.log('frommm::', this.props.navigation.state.params.from);
        }
      } else {
        await notification.pushNotification(this._handleNotificationOpen);
      }
    }
  };
  requestCameraPermission = async () => {
    try {
      await this.setState({isVisibleLoading: true});
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'App location Permission',
          message: ' App needs access to your location ' + '',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('loc permission');
        await this.setState({isVisibleLoading: false});
        await this.onRegionChange();
      } else {
        await this.setState({isVisibleLoading: false});
        console.log('loc permission denied');
      }
    } catch (err) {
      await this.setState({isVisibleLoading: false});
      console.warn(err);
    }
  };
  onRegionChange = async () => {
    Geolocation.getCurrentPosition(
      position => {
        this.onUserPinDragEnd(position.coords);
      },
      error => {
        this.setState({error: error.message}),
          console.log('errorrrrrrrrr:', error.message);
      },
      {enableHighAccuracy: false, timeout: 200000, maximumAge: 10000},
    );
  };
  onUserPinDragEnd = async value => {
    await this.setState({
      coordinates: {
        latitude: value.latitude,
        longitude: value.longitude,
      },
      query: `searchType=food&longitude=${value.longitude}&latitude=${
        value.latitude
      }`,
    });
    await utility.setItem('coordinates', this.state.coordinates);
    await this.getFood();
  };
  onTokenUpdate = async () => {
    await notification.pushNotification(this._handleNotificationOpen);
    await this.onUserUpdate();
  };
  onLocation = async () => {
    await this.props.navigation.push('Map', {
      refersh: this.onGetAddress,
      from: 'home',
    });
  };
  onGetAddress = async () => {
    await this.setState({selectedIndex: 0});
    await this.onTokenUpdate();
  };
  onMap = async () => {
    await this.props.navigation.push('Map', {
      refersh: this.onRefersh,
      from: 'home',
    });
  };
  onRefersh = async () => {
    await this.setState({selectedIndex: 0});
    if (this.state.isSkipped) {
      let coordinates = await utility.getItem('coordinates');
      await this.onUserPinDragEnd(coordinates);
    } else {
      await this.setState({
        query: `forYou=true&userId=${this.state.userId}`,
      });
      await this.getFood();
    }
  };
  onLoginFirst = async () => {
    await this.props.navigation.navigate('Login');
  };
  _handleNotificationOpen = async data => {
    console.log('notification data datattatatatt:', data);

    if (data.notificationType == 'Chat') {
      let tempData = JSON.parse(data.chatData);
      await this.props.navigation.navigate('ChatScreen', {
        from: 'outerNotification',
        item: tempData,
        socket: this.socket,
      });
    } else if (data.notificationType == 'New food') {
      await this.props.navigation.navigate('FoodDetails', {
        from: 'outerNotification',
        foodId: data.id,
      });
    } else if (data.notificationType == 'Order delivered') {
      await this.props.navigation.navigate('OrderDetails', {
        from: 'outerNotification',
        for: 'delivered',
        orderId: data.id,
        status: 'delivered',
      });
    } else if (data.notificationType == 'Order received') {
      await this.props.navigation.navigate('OrderDetails', {
        from: 'outerNotification',
        for: 'pending',
        orderId: data.id,
        status: 'pending',
      });
    } else {
      await this.props.navigation.navigate('OrderDetails', {
        from: 'outerNotification',
        for: 'confirmed',
        orderId: data.id,
        status: 'confirmed',
      });
    }
  };
  onAddressStatusUpdate = async () => {
    await utility.setItem('isAddressExists', this.state.isAddressExists);
  };
  onUserUpdate = async () => {
    let fcmToken = await utility.getItem('fcmToken');
    await this.setState({isVisibleLoading: true});
    let body = {
      firebaseToken: fcmToken,
    };
    try {
      let response = Service.putDataApi(
        `users/${this.state.userId}`,
        body,
        this.state.userToken,
      );
      response
        .then(res => {
          if (res.data) {
            this.setState({isVisibleLoading: false});
            console.log('token  update successfully');
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onListItem = async index => {
    if (index == 0) {
      if (this.state.isSkipped) {
        await this.setState({query: '', foods: [], noDataExist: true});
      } else {
        await this.setState({
          query: `forYou=true&userId=${this.state.userId}`,
        });
        await this.getFood();
      }
    }
    if (index == 1) {
      this.state.isSkipped
        ? await this.setState({
            query: `searchType=food&longitude=${
              this.state.coordinates.longitude
            }&latitude=${this.state.coordinates.latitude}`,
          })
        : await this.setState({
            query: `nearByYou=true&userId=${this.state.userId}`,
          });
      await this.getFood();
    }
    if (index == 2) {
      await this.setState({query: 'isFoodFree=true&&searchType=food'});
      await this.getFood();
    }
    if (index == 3) {
      let lastSearchQuery = await utility.getItem('lastSearchQuery');
      console.log('lastsearch query', lastSearchQuery);
      if (
        lastSearchQuery !== null &&
        lastSearchQuery !== undefined &&
        lastSearchQuery !== ''
      ) {
        await this.setState({query: lastSearchQuery});
        await this.getFood();
      } else {
        await this.setState({noDataExist: true});
      }
    }
    if (index == 4) {
      if (this.state.isSkipped) {
        await this.setState({query: '', foods: [], noDataExist: true});
      } else {
        await this.setState({
          query: `isfollow=true&userId=${this.state.userId}`,
        });
        await this.getFood();
      }
    }
    if (index == 5) {
      await this.setState({query: 'type=langar&searchType=food'});
      await this.getFood();
    }
    if (index == 6) {
      await this.setState({query: `cuisineName=lunch`});
      await this.getFood();
    }
    if (index == 7) {
      await this.setState({query: `cuisineName=breakfast`});
      await this.getFood();
    }
    if (index == 8) {
      await this.setState({query: `cuisineName=dinner`});
      await this.getFood();
    }
    await this.setState({selectedIndex: index});
  };

  getFood = async () => {
    await this.setState({isVisibleLoading: true, foods: []});
    try {
      let url;
      if (
        this.state.query == '' ||
        this.state.query == null ||
        this.state.query == undefined
      ) {
        url = Url.GET_FOODS;
      } else {
        url = Url.SEARCH_FOOD + `?${this.state.query}`;
      }
      let response = Service.getDataApi(url, '');
      response
        .then(res => {
          if (res.data) {
            if (res.data.length != 0) {
              this.setState({noDataExist: false});
              let tempFoods = [];

              for (let i = 0; i < res.data.length; i++) {
                let image;
                if (res.data[i].images) {
                  image = res.data[i].images[0].resize_url;
                }
                if (res.data[i].type != 'langar') {
                  if (res.data[i].quantity > 0) {
                    tempFoods.push({
                      id: res.data[i]._id,
                      type: res.data[i].type,
                      name: res.data[i].name,
                      price: res.data[i].price,
                      quantity: res.data[i].quantity,
                      address: res.data[i].address.line,
                      image: image,
                    });
                  }
                } else {
                  tempFoods.push({
                    id: res.data[i]._id,
                    type: res.data[i].type,
                    name: res.data[i].name,
                    price: res.data[i].price,
                    quantity: res.data[i].quantity,
                    address: res.data[i].address.line,
                    image: image,
                  });
                }
              }
              this.setState({
                foods: tempFoods,
                isVisibleLoading: false,
              });
            } else {
              this.setState({noDataExist: true, isVisibleLoading: false});
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('if no data in response:', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('try-catch error:', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onBack = async () => {
    const rembemberMe = await utility.getItem('rembemberMe');
    if (rembemberMe == false) {
      await utility.removeAuthKey('token');
      await utility.removeAuthKey('userId');
    }
    BackHandler.exitApp();
    return true;
  };
  onSearch = async () => {
    this.props.navigation.navigate('SearchName', {
      name: this.state.name,
      from: 'home',
    });
    await this.setState({name: ''});
  };
  closeDialog = async () => {
    this.setState({isVisible: false});
  };
  onNotification = async () => {
    if (this.state.isSkipped) {
      await utility.showAlert('Please login first.', this.onLoginFirst);
    } else {
      await this.props.navigation.navigate('Notifications', {from: 'home'});
    }
  };
  showDetail = async data => {
    this.props.navigation.navigate('FoodDetails', {
      foodId: data.id,
      from: 'home',
    });
  };
  render() {
    const {
      container,
      container_width,
      top_container,
      row,
      list_container,
      icons,
      center_align,
      badge_style,
      selected_color,
      badge_text_style,
      filter_text,
      filter_container,
      unselected_color,
      filters,
      column,
      between_spacing,
      around_spacing,
      search_icon,
      search_input,
      search_container,
      centered_text,
      image,
      heading_container,
      heading_text,
      yellow_color,
      free_text,
      colored_text,
      grey_text,
      row_list,
      photo_continer,
      list_height,
    } = styles;
    return (
      <HandleBack onBack={this.onBack}>
        <View style={[container, between_spacing]}>
          <View style={top_container}>
            <View>
              <View style={[row, between_spacing, container_width]}>
                <TouchableOpacity activeOpacity={0.7} onPress={this.onMap}>
                  <Image
                    resizeMode="contain"
                    source={require('../../assets/location.png')}
                    style={icons}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.onSearch}>
                  <View style={[search_container, row, around_spacing]}>
                    <Image
                      resizeMode="contain"
                      source={require('../../assets/search.png')}
                      style={search_icon}
                    />
                    <TextInput
                      placeholder="Search food"
                      style={search_input}
                      onChangeText={name => this.setState({name})}
                      value={this.state.name}
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={this.onNotification}>
                  <View style={[row, center_align]}>
                    <Image
                      resizeMode="contain"
                      source={require('../../assets/notification_solid_yellow.png')}
                      style={icons}
                    />
                    {this.state.notificationCount !== 0 ? (
                      <Badge
                        value={this.state.notificationCount}
                        status="success"
                        badgeStyle={badge_style}
                        textStyle={badge_text_style}
                      />
                    ) : null}
                  </View>
                </TouchableOpacity>
              </View>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <View style={[row, filter_container]}>
                  {this.state.filtersList.map(item => {
                    let index = this.state.filtersList.indexOf(item);
                    return (
                      <TouchableOpacity onPress={() => this.onListItem(index)}>
                        <View
                          style={
                            this.state.selectedIndex == index
                              ? [filters, selected_color]
                              : [filters, unselected_color]
                          }>
                          <Text style={filter_text}>{item.name}</Text>
                        </View>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </ScrollView>
            </View>
          </View>
          {!this.state.noDataExist ? (
            <View style={list_height}>
              <ScrollView>
                <View style={[list_container, row, row_list]}>
                  {this.state.foods.map(data => {
                    return (
                      <TouchableOpacity
                        activeOpacity={0.6}
                        onPress={() => this.showDetail(data)}>
                        <View style={row}>
                          <View style={[column]}>
                            <View style={photo_continer}>
                              <Image
                                resizeMode="stretch"
                                source={{uri: data.image}}
                                style={image}
                              />
                            </View>
                            <View style={[heading_container, column]}>
                              <Text style={heading_text} numberOfLines={1}>
                                {data.name}
                              </Text>
                              <View style={[row, between_spacing]}>
                                {data.type == 'langar' ? (
                                  <Text style={yellow_color}>Langar</Text>
                                ) : data.price == 0 ? (
                                  <Text style={free_text}>Free</Text>
                                ) : (
                                  <Text style={colored_text}>
                                    Rs {data.price}
                                  </Text>
                                )}
                                {data.type != 'langar' ? (
                                  <Text style={grey_text}>
                                    {data.quantity} Items
                                  </Text>
                                ) : (
                                  <View />
                                )}
                              </View>
                              <Text style={grey_text} numberOfLines={1}>
                                {data.address}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </ScrollView>
            </View>
          ) : (
            <View style={[list_height, centered_text]}>
              <Text style={{textAlign: 'center'}}>
                No food found for today, add food for share
              </Text>
            </View>
          )}
          <Loader isVisibleLoading={this.state.isVisibleLoading} />
        </View>
      </HandleBack>
    );
  }
}

// handleScroll = async event => {
//   let scrollHeight = event.nativeEvent.contentSize.height;
//   let scrolledViewHeight = event.nativeEvent.contentOffset.y;
//   console.log('scrollHeight', scrollHeight);
//   console.log('scrolledViewHeight', scrolledViewHeight);
//   let average = (scrolledViewHeight / scrollHeight) * 100;
//   console.log('average::', average);
//   if (average > 20) {
//     let page = this.state.pageNo;
//     page++;
//     console.log('pageeeeeeeeee:::', page);
//     await this.setState({pageNo: page});
//     await this.setState({
//       query: `nearByYou=true&userId=${this.state.userId}&pageNo=${
//         this.state.pageNo
//       }&items=5`,
//     });
//     await this.loadMoreData();
//   }
// };
// loadMoreData = async () => {
//   await this.setState({isVisibleLoading: true});
//   try {
//     let url;
//     if (
//       this.state.query == '' ||
//       this.state.query == null ||
//       this.state.query == undefined
//     ) {
//       url = Url.GET_FOODS;
//     } else {
//       url = Url.SEARCH_FOOD + `?${this.state.query}`;
//     }
//     let response = Service.getDataApi(url, '');
//     response
//       .then(res => {
//         if (res.data) {
//           if (res.data.length != 0) {
//             let tempFoods = [...this.state.foods];

//             for (let i = 0; i < res.data.length; i++) {
//               let image;
//               if (res.data[i].images) {
//                 image = res.data[i].images[0].resize_url;
//               }
//               tempFoods.push({
//                 id: res.data[i]._id,
//                 type: res.data[i].type,
//                 name: res.data[i].name,
//                 price: res.data[i].price,
//                 quantity: res.data[i].quantity,
//                 address: res.data[i].address.line,
//                 image: image,
//               });
//             }
//             this.setState({
//               foods: tempFoods,
//               isVisibleLoading: false,
//             });
//           } else {
//             this.setState({isVisibleLoading: false});
//           }
//         } else {
//           this.setState({isVisibleLoading: false});
//           console.log('if no data in response:', res.error);
//         }
//       })
//       .catch(error => {
//         this.setState({isVisibleLoading: false});
//         console.log('try-catch error:', error.error);
//         utility.alert('Something went wrong');
//       });
//   } catch (err) {
//     this.setState({isVisibleLoading: false});
//     console.log('another problem:', err);
//     utility.alert('Something went wrong');
//   }
// };
