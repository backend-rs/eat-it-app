import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  container: {
    height: '100%',
    width: '100%',
  },
  container_width: {
    width: wp(100),
    paddingHorizontal: wp(1),
    alignSelf: 'center',
  },
  top_container: {
    height: hp(15),
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  list_container: {
    width: wp(100),
  },
  icons: {
    width: wp(8.2),
    height: hp(8.2),
  },
  center_align: {
    alignItems: 'center',
  },
  badge_style: {
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: colors.primaryColor,
    marginTop: hp(-3),
    marginLeft: wp(-4),
  },
  selected_color: {
    backgroundColor: colors.gradientFirstColor,
  },
  unselected_color: {
    backgroundColor: colors.unselectedFilter,
  },
  badge_text_style: {
    fontSize: fonts.Font_MIN,
  },
  filter_text: {
    fontSize: 12,
  },
  filter_container: {
    marginVertical: hp(1.6),
    paddingHorizontal: wp(2),
  },
  filters: {
    height: hp(3.8),
    width: 'auto',
    borderRadius: 15,
    padding: 12,
    alignItems: 'center',
    justifyContent: 'center',
    // marginRight: wp(1.2),/
    marginHorizontal: wp(0.7),
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  around_spacing: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  search_container: {
    width: wp(70),
    height: hp(5.6),
    borderWidth: 1,
    borderColor: colors.primaryColor,
    borderRadius: 25,
    backgroundColor: '#ECECEC',
  },
  search_icon: {
    width: wp(6),
    height: hp(6),
  },
  search_input: {
    width: wp(52),
    padding: 0,
  },

  inner_container: {
    width: wp(96),
    alignSelf: 'center',
  },
  row_list: {
    flexWrap: 'wrap',
  },
  centered_text: {justifyContent: 'center', alignItems: 'center'},
  image: {
    height: '100%',
    width: '100%',
    borderRadius: 8,
  },
  heading_container: {
    marginHorizontal: wp(3),
    marginBottom: hp(2.2),
    width: wp(26),
  },
  heading_text: {
    fontSize: fonts.FONT_NORMAL,
    fontWeight: fonts.FONT_BOLD,
    width: wp(28),
    textWrap: 'wrap',
    textTransform: 'capitalize',
  },
  colored_text: {
    fontSize: 12,
    fontWeight: fonts.FONT_BOLD,
    color: colors.primaryColor,
  },
  yellow_color: {
    fontSize: 12,
    fontWeight: fonts.FONT_BOLD,
    color: '#FED704',
  },
  free_text: {
    fontSize: 12,
    fontWeight: fonts.FONT_BOLD,
    color: 'green',
  },
  grey_text: {
    fontSize: fonts.Font_MIN,
    color: colors.fieldsColor,
  },
  photo_continer: {
    height: hp(14.6),
    width: wp(27.4),
    borderWidth: 1.4,
    borderRadius: 4,
    borderColor: colors.primaryColor,
    backgroundColor: 'white',
    marginBottom: hp(1),
    marginHorizontal: wp(2),
  },
  list_height: {
    height: hp(83),
    paddingBottom: hp(10),
  },
};

export default styles;
