import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  container: {
    width: '100%',
    height: '100%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  arrow: {
    height: hp(4.5),
    width: hp(4.5),
  },
  forward_container: {
    height: hp(7.6),
    width: hp(7.6),
    borderRadius: hp(7.6) / 2,
    backgroundColor: '#FFBA09',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 6,
  },
  around_spacing: {justifyContent: 'space-around'},
  cancel_style: {
    alignSelf: 'center',
    textDecorationLine: 'underline',
    marginTop: hp(2),
    color: colors.primaryColor,
    fontWeight: fonts.FONT_BOLD,
    fontSize: fonts.FONT_TEXT,
  },
  button_container: {
    alignSelf: 'flex-end',
    justifyContent: 'space-between',
    width: wp(50),
  },
  bottom_margin: {
    marginBottom: hp(6),
  },
  vertical_margin: {
    paddingVertical: hp(2),
    paddingHorizontal: wp(4),
  },
  dialog_container: {
    width: wp(74),
    elevation: 6,
    backgroundColor: '#ECECEC',
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  text_style: {
    fontSize: fonts.FONT_TEXT,
    fontWeight: fonts.FONT_BOLD,
    textAlign: 'center',
  },
};
export default styles;
