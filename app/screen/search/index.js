import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import styles from './style';
import * as Service from '../../api/services';
import * as Url from '../../constants/urls';
import Loader from '../../components/Loader';

export default class search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cuisions: [],
      name: '',
      isVisibleLoading: false,
      dataExist: true,
      isLangar: false,
    };
  }
  componentDidMount = async () => {
    this.getCuisions();
  };

  onFilter = async () => {
    this.props.navigation.navigate('Filter');
  };

  getCuisions = async () => {
    this.setState({cuisions: [], isVisibleLoading: true});
    try {
      let response = Service.getDataApi(Url.GET_CUISIONS, '');
      response
        .then(res => {
          if (res.data) {
            if (res.data.length != 0) {
              this.setState({dataExist: true});
              let tempCuisions = [];
              for (let i = 0; i < res.data.length; i++) {
                let image;
                if (res.data[i].image) {
                  image = res.data[i].image.resize_url;
                }
                tempCuisions.push({
                  id: res.data[i]._id,
                  name:
                    res.data[i].name.slice(0, 1).toUpperCase() +
                    res.data[i].name.slice(1, res.data[i].name.length),
                  image: image,
                });
              }
              this.setState({
                cuisions: tempCuisions,
                isLangar: true,
                isVisibleLoading: false,
              });
            } else {
              this.setState({isVisibleLoading: false, dataExist: false});
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found:', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('try-catch error:', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onNameChange(name) {
    if (name == '') {
      this.setState({cuisions: [], name: ''});
      this.getCuisions();
    } else {
      this.setState({name});
      this.setState({
        isLangar: false,
        query: `name=${this.state.name}&searchType=cuisine`,
      });
      this.getFood();
    }
  }
  getCuisionFood = async (cuisineId, name) => {
    await this.props.navigation.navigate('SearchName', {
      cuisineId: cuisineId,
      cuisineName: name,
      from: 'search',
    });
  };
  getLangarFood = async type => {
    await this.props.navigation.navigate('SearchName', {from: 'langar'});
  };
  getFood = async () => {
    await this.setState({cuisions: [], isVisibleLoading: true});
    try {
      let response = Service.getDataApi(
        Url.SEARCH_FOOD + `?${this.state.query}`,
        '',
      );
      response
        .then(res => {
          if (res.data) {
            if (res.data.length != 0) {
              this.setState({dataExist: true});
              let tempProducts = [];
              for (let i = 0; i < res.data.length; i++) {
                let image;
                if (res.data[i].image) {
                  image = res.data[i].image.url;
                }
                tempProducts.push({
                  id: res.data[i]._id,
                  name:
                    res.data[i].name.slice(0, 1).toUpperCase() +
                    res.data[i].name.slice(1, res.data[i].name.length),
                  image: image,
                });
              }
              this.setState({cuisions: tempProducts, isVisibleLoading: false});
            } else {
              this.setState({isVisibleLoading: false, dataExist: false});
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found:', res);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('try-catch error:', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  render() {
    const {
      container,
      column,
      search_container,
      top_container,
      row,
      between_spacing,
      around_spacing,
      search_input,
      search_icon,
      inner_container,
      row_list,
      photo_continer,
      centered_text,
      photo_style,
      cuision_text,
      list_height,
    } = styles;
    return (
      <View style={[container, column, between_spacing]}>
        <View>
          <View style={[row, around_spacing, top_container]}>
            <View style={[search_container, row, around_spacing]}>
              <Image
                resizeMode="contain"
                source={require('../../assets/search.png')}
                style={search_icon}
              />
              <TextInput
                placeholder="Search cuisines"
                style={search_input}
                onChangeText={name => this.onNameChange(name)}
                value={this.state.name}
              />
            </View>
          </View>
          {this.state.dataExist ? (
            <View style={list_height}>
              <ScrollView>
                <View style={[inner_container, row, row_list]}>
                  {this.state.isLangar ? (
                    <TouchableOpacity
                      activeOpacity={0.7}
                      onPress={() => this.getLangarFood('langar')}>
                      <View>
                        <View style={row}>
                          <View style={[photo_continer]}>
                            <Image
                              resizeMode="cover"
                              source={require('../../assets/burger.jpg')}
                              style={photo_style}
                            />
                          </View>
                        </View>
                        <Text style={cuision_text}>Langar</Text>
                      </View>
                    </TouchableOpacity>
                  ) : (
                    <View />
                  )}
                  {this.state.cuisions.map(item => {
                    return (
                      <TouchableOpacity
                        onPress={() => this.getCuisionFood(item.id, item.name)}>
                        <View>
                          <View style={row}>
                            <View style={[photo_continer]}>
                              <Image
                                resizeMode="cover"
                                source={{uri: item.image}}
                                style={photo_style}
                              />
                            </View>
                          </View>
                          <Text style={cuision_text}>{item.name}</Text>
                        </View>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </ScrollView>
            </View>
          ) : (
            <View style={[list_height, column, centered_text]}>
              <Text style={{textAlign: 'center'}}>No food found</Text>
            </View>
          )}
          <Loader isVisibleLoading={this.state.isVisibleLoading} />
        </View>
      </View>
    );
  }
}
