import React, {Component} from 'react';
import {Animated, View, ImageBackground, Easing} from 'react-native';
import styles from './style';
import * as utility from '../../utility/index';
import {heightPercentageToDP as hp} from '../../utility/index';

export default class splash extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.animatedValue = new Animated.Value(0);
  }
  componentDidMount = async () => {
    await this.handleAnimation();
    this.timeoutHandle = setTimeout(() => {
      this.retrieveData();
    }, 1200);
  };

  retrieveData = async () => {
    try {
      var skipped = await utility.getItem('isSkipped');
      var token = await utility.getToken('token');
      if (
        skipped == null ||
        skipped == '' ||
        skipped == undefined ||
        skipped == false
      ) {
        if (token == null || token == '' || token == undefined) {
          this.props.navigation.navigate('Login');
        } else {
          this.props.navigation.navigate('tab1');
        }
      } else {
        this.props.navigation.navigate('tab1');
      }
    } catch (error) {
      utility.alert(error);
    }
  };

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }
  handleAnimation = () => {
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 1400,
      useNativeDriver: true,
      easing: Easing.ease,
    }).start();
  };

  render() {
    const {container, logo} = styles;
    return (
      <View>
        <ImageBackground
          source={require('../../assets/background.png')}
          style={container}
          resizeMode="cover">
          <Animated.Image
            source={require('../../assets/eatit_logo.png')}
            resizeMode="contain"
            style={{
              position: 'absolute',
              top: hp(40),
              height: hp(3),
              width: hp(2.6),
              transform: [
                {
                  translateX: this.animatedValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 0],
                  }),
                },
                {
                  translateY: this.animatedValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 25],
                  }),
                },
                {
                  scaleX: this.animatedValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 15],
                  }),
                },
                {
                  scaleY: this.animatedValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 15],
                  }),
                },
              ],
            }}
          />
        </ImageBackground>
      </View>
    );
  }
}
