import React, {Component} from 'react';
import {View, Text} from 'react-native';
import styles from './style';
import Header from '../../components/header';

export default class customerCare extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  onBack = async () => {
    await this.props.navigation.goBack();
  };
  render() {
    const {row, content_container, bottom_margin, list_headings} = styles;
    return (
      <View>
        <Header title={'Customer care'} onBack={this.onBack} />
        <View style={[content_container]}>
          <View style={[row, bottom_margin]}>
            <Text style={list_headings}>Email : </Text>
            <Text>info@eatit.co.in</Text>
          </View>
          <View style={row}>
            <Text style={list_headings}>Phone : </Text>
            <Text>9340236745</Text>
          </View>
        </View>
      </View>
    );
  }
}
