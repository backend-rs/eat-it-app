import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  content_container: {
    width: wp(92.8),
    alignSelf: 'center',
    marginTop:hp(4)
  },
  bottom_margin: {
    marginBottom: hp(3.4),
  },
  list_headings: {
    fontSize: fonts.FONT_TEXT,
    fontWeight: fonts.FONT_BOLD,
    color: colors.primaryColor,
  },
};
export default styles;
