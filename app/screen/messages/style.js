import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  container: {
    width: '100%',
    height: '92%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  spacing: {
    marginTop: hp(2.4),
  },
  horizontal_line: {
    borderBottomWidth: 1,
    borderColor: '#B5B5B5',
    width: wp(100),
  },
  list_heading: {
    fontSize: fonts.FONT_TEXT,
    fontWeight: fonts.FONT_BOLD,
    textTransform: 'capitalize',
  },
  code_style: {
    color: colors.primaryColor,
    marginRight: wp(1.8),
  },
  message_style: {
    fontSize: 12,
    fontWeight: fonts.FONT_BOLD,
    color: colors.greyText,
  },
  time_style: {
    fontSize: fonts.FONT_NORMAL,
    color: colors.greyText,
    marginRight: wp(1.8),
  },
  list_padding: {
    paddingVertical: hp(1.2),
  },
  phone_icon: {
    height: hp(3),
    width: hp(3),
    marginLeft: wp(3),
  },
  centered_text: {
    alignItems: 'center',
    paddingHorizontal: wp(5),
  },
  center_text: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  list_container: {
    width: wp(78),
    alignSelf: 'center',
  },
  list_height: {height: hp(86)},
};
export default styles;
