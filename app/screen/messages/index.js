import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Linking,
  ScrollView,
} from 'react-native';
import styles from './style';
import SocketIOClient from 'socket.io-client';
import * as utility from '../../utility/index';
import moment from 'moment';
import Loader from '../../components/Loader';
import Header from '../../components/header';
import * as Service from '../../api/services';
import * as Url from '../../constants/urls';

export default class messages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userToken: '',
      userId: '',
      username: '',
      chatList: [],
      isDataExists: true,
      isVisibleLoading: false,
    };
    {
      global.user !== undefined ? (this.socket = SocketIOClient(Url.URL)) : '';
    }
    {
      global.user !== undefined
        ? this.socket.on('connect', () => {
            console.log('socketio chat connected.', global.user.firstName);
            this.socket.emit(
              'set-user-data',
              global.user.firstName,
              global.user._id,
            );
          })
        : '';
    }

    this.refersh = this.refersh.bind(this);
  }
  componentDidMount = async () => {
    const token = await utility.getToken('token');
    const userId = await utility.getItem('userId');
    const username = await utility.getItem('name');
    await this.setState({userToken: token, userId: userId, username: username});
    await this.getUsers();
  };
  refersh = async () => {
    this.socket = SocketIOClient(Url.URL);

    this.socket.on('connect', () => {
      console.log('socketio chat connected.', global.user.firstName);
      this.socket.emit('set-user-data', global.user.firstName, global.user._id);
    });
    const token = await utility.getToken('token');
    const userId = await utility.getItem('userId');
    const username = await utility.getItem('name');
    await this.setState({userToken: token, userId: userId, username: username});
    await this.getUsers();
  };
  onBack = async () => {
    await this.props.navigation.goBack();
  };

  getUsers = async () => {
    await this.setState({isVisibleLoading: true});
    try {
      let response = Service.getDataApi(
        `users/socketUsers?userId=${this.state.userId}`,
        this.state.userToken,
      );
      response
        .then(res => {
          if (res.data && res.data.length != 0) {
            console.log('res.data', res.data);
            let data = res.data;
            this.setState({
              chatList: data,
              isVisibleLoading: false,
              isDataExists: true,
            });
          } else {
            this.setState({isDataExists: false, isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isDataExists: false, isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isDataExists: false, isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onlistItem = async item => {
    await this.props.navigation.navigate('ChatScreen', {
      item: item,
      socket: this.socket,
      onRefersh: this.refersh,
    });
  };
  render() {
    const {
      container,
      row,
      between_spacing,
      spacing,
      horizontal_line,
      list_heading,
      code_style,
      message_style,
      time_style,
      list_padding,
      phone_icon,
      centered_text,
      list_container,
      center_text,
      list_height,
    } = styles;
    return (
      <>
        <Header title={'Messages'} onBack={this.onBack} />
        <View style={[container, spacing]}>
          {this.state.isDataExists ? (
            <View style={list_height}>
              <ScrollView>
                {this.state.chatList.map(chat => {
                  return (
                    <>
                      <View style={[row, centered_text]}>
                        <TouchableOpacity onPress={() => this.onlistItem(chat)}>
                          <View style={[list_container, list_padding]}>
                            <View style={[row, between_spacing]}>
                              <Text style={list_heading}>
                                {chat.username + ' ' + chat.lastname}
                              </Text>
                              <Text style={[list_heading, code_style]}>
                                #{chat.orderId}
                              </Text>
                            </View>

                            <View style={[row, between_spacing]}>
                              <Text style={message_style}>
                                {chat.msg != ' ' ||
                                chat.msg != null ||
                                chat.msg != undefined
                                  ? chat.msg
                                  : 'New'}
                              </Text>
                              <Text style={time_style}>
                                {chat.time != ''
                                  ? moment(chat.time).format('hh:mm a')
                                  : ''}
                              </Text>
                            </View>
                          </View>
                        </TouchableOpacity>
                        {chat.isPhone ? (
                          <TouchableOpacity
                            onPress={() => {
                              Linking.openURL(`tel:${chat.phone}`);
                            }}>
                            <Image
                              source={require('../../assets/phone.png')}
                              style={phone_icon}
                            />
                          </TouchableOpacity>
                        ) : (
                          <View />
                        )}
                        {chat.isSms ? (
                          <TouchableOpacity
                            onPress={() => {
                              Linking.openURL(
                                `sms:${chat.phone}?body=Hii can you please`,
                              );
                            }}>
                            <Image
                              source={require('../../assets/email.png')}
                              style={phone_icon}
                            />
                          </TouchableOpacity>
                        ) : (
                          <View />
                        )}
                      </View>
                      <View style={horizontal_line} />
                    </>
                  );
                })}
              </ScrollView>
            </View>
          ) : (
            <View style={[[list_height, center_text]]}>
              <Text>No chat list found</Text>
            </View>
          )}
          <Loader isVisibleLoading={this.state.isVisibleLoading} />
        </View>
      </>
    );
  }
}

// getUsers = async () => {
//   console.log('userList');
//   await this.setState({isVisibleLoading: true});
//   await this.socket.on('onlineStack', userList => {
//     console.log('userrrrrlisttst', userList);
//     if (userList && userList.length != 0) {
//       let dataList = userList.filter(function(item) {
//         return global.user.firstName != item.username;
//       });
//       this.setState({
//         chatList: dataList,
//         isVisibleLoading: false,
//       });
//       dataList.length != 0
//         ? this.setState({isDataExists: true})
//         : this.setState({isDataExists: false});
//     } else {
//       this.setState({
//         isDataExists: false,
//         isVisibleLoading: false,
//       });
//     }
//   });
// };
