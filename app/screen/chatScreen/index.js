import React, {Component} from 'react';
import {View, Text, Image, Animated} from 'react-native';
import styles from './style';
import {heightPercentageToDP as hp} from '../../utility/index';
import {GiftedChat, Bubble, Send, InputToolbar} from 'react-native-gifted-chat';
import {v4 as uuidv4} from 'uuid';
import Loader from '../../components/Loader';
import AndroidKeyboardAdjust from 'react-native-android-keyboard-adjust';
import Header from '../../components/header';
import * as utility from '../../utility/index';
import {NavigationActions, StackActions} from 'react-navigation';

var timeoutVar = null;

export default class ChatScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyboardOffset: 0,
      code: '',
      message: '',
      height: hp(82),
      iskeyboard: false,
      messages: [],
      typing: false,
      timestamp: 'not timestamp yet',
      roomId: '',
      isLoadingEarlier: false,
      top: new Animated.Value(hp(0)),
      margin: 0,
      isKeyBoardOpen: false,
      isExpired: true,
      isVisibleLoading: false,
      showHeader: true,
      calling: false,
      userToken: '',
    };

    this.onSend = this.onSend.bind(this);
    this.onInputTextChanged = this.onInputTextChanged.bind(this);
    this.socket = this.props.navigation.state.params.socket;
    // this.socket.removeAllListeners();
    this.userTo = this.props.navigation.state.params.item;
    // console.log('userTooooooooo:', this.userTo);
    // this.setRoom(this.userTo, global.user);
    // this.onRoomSet();
    // this.getOldChats();
    this.onReceiveMessage();
  }
  componentWillMount() {
    this.onSenderTyping();
  }
  componentDidMount = async () => {
    this.userTo = this.props.navigation.state.params.item;
    let userToken = await utility.getToken('token');
    await this.setState({userToken: userToken});
    let from;
    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params.from) {
        from = this.props.navigation.state.params.from;
        await this.setState({from: from});
      } else {
        await this.setState({from: ''});
      }
    }
    if (this.state.from == 'outerNotification') {
      if (
        this.state.userToken == null ||
        this.state.userToken == undefined ||
        this.state.userToken == ''
      ) {
        await utility.showAlert('Please login first.', this.onLoginFirst);
        await this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'BottomTab'})],
          }),
        );
        await this.props.navigation.navigate('tab1', {from: 'foodDetails'});
      }
    }
    if (this.userTo) {
      await utility.setItem('activeChat', this.userTo.orderId);
      await this.setState({isExpired: this.userTo.isExpired});
    }
    await this.setRoom(this.userTo, global.user);
    await this.onRoomSet();
    await this.getOldChats();

    AndroidKeyboardAdjust.setAdjustResize();
  };
  componentWillUnmount = async () => {
    await utility.removeAuthKey('activeChat');
    AndroidKeyboardAdjust.setAdjustPan();
  };
  onLoginFirst = async () => {
    await this.props.navigation.navigate('Login');
  };
  setRoom = async (userTo, userFrom) => {
    var currentRoom =
      userFrom.firstName +
      userTo.orderId +
      '-' +
      userTo.username +
      userTo.orderId;
    var reverseRoom =
      userTo.username +
      userTo.orderId +
      '-' +
      userFrom.firstName +
      userTo.orderId;
    this.socket.emit('set-room', {name1: currentRoom, name2: reverseRoom});
    console.log('set-room emitted successfully');
  };
  onRoomSet = async () => {
    console.log('inside room');
    this.socket.on('set-room', roomId => {
      console.log('RoomID :: ', roomId);
      this.setState({roomId: roomId});
      // this.getOldChats();
      this.socket.emit('old-chats', {
        room: roomId,
        username: global.user.firstName,
        msgCount: 1,
      });
    });
  };
  getOldChats = async () => {
    let emptyChat = [];
    await this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, emptyChat),
      isVisibleLoading: true,
    }));
    this.socket.on('old-chats', data => {
      if (data.room == this.state.roomId) {
        if (data.result.length != 0) {
          let tempMessages = [];
          for (let item of data.result) {
            tempMessages.push({
              _id: item.chatId,
              text: item.msg,
              createdAt: item.date,
              user: {
                _id: item.msgFrom,
              },
            });
          }
          // console.log('tempMessages', tempMessages);
          this.setMessages(tempMessages);
        } else {
          this.setState({isVisibleLoading: false});
        }
      }
    });
  };
  setMessages = async tempMessages => {
    await this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, tempMessages),
    }));
    await this.setState({isVisibleLoading: false, isLoadingEarlier: true});
    console.log('isVisibleLoading', this.state.isVisibleLoading);
  };
  onReceiveMessage = async () => {
    console.log('Checking for incoming message');
    this.socket.on('chat-msg', data => {
      console.log('incoming message data', data);
      if (data.msgFrom !== global.user.firstName) {
        let messageID = uuidv4();
        console.log('message id :: ', messageID);
        const incomingMessage = {
          _id: messageID,
          text: data.msg,
          createdAt: data.date,
          user: {
            _id: data.msgFrom,
          },
        };
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, incomingMessage),
        }));
      }
    });
  };
  onSend(messages = []) {
    console.log('on Send called');
    console.log('Messages', messages);
    console.log('user to id :: ', this.userTo);
    this.socket.emit('chat-msg', {
      msgFromId: global.user._id,
      msgToId: this.userTo.userId,
      orderId: this.userTo.orderId,
      msg: messages[0].text,
      msgTo: this.userTo.username,
      date: Date.now(),
    });
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
  }
  onInputTextChanged() {
    this.socket.emit('typing');
  }
  renderFooter = () => {
    if (this.state.typing) {
      return (
        <View>
          <Text>Typing...</Text>
        </View>
      );
    }
    return null;
  };
  onSenderTyping() {
    this.socket.on('typing', data => {
      console.log('data', data);
      if (!this.state.typing) {
        this.setState({
          typing: true,
        });
      } else {
        clearTimeout(timeoutVar);
      }
      this.timeoutVar = setTimeout(() => {
        this.setState({
          typing: false,
        });
      }, 2000);
    });
  }
  onBack = async () => {
    await this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, []),
      isVisibleLoading: false,
    }));
    await this.socket.removeAllListeners();
    await utility.removeAuthKey('activeChat');
    AndroidKeyboardAdjust.setAdjustPan();
    if (this.state.from == 'outerNotification') {
      await this.props.navigation.navigate('tab1');
    } else {
      await this.props.navigation.state.params.onRefersh();
      await this.props.navigation.navigate('Messages');
    }
  };
  renderSend = props => {
    return (
      <Send {...props}>
        <View style={styles.next_arrow_container}>
          <Image
            resizeMode="contain"
            source={require('../../assets/send_icon.png')}
            style={styles.icons}
          />
        </View>
      </Send>
    );
  };
  renderBubble(props) {
    return (
      <Bubble
        {...props}
        textStyle={{
          right: {color: 'black'},
          left: {color: 'black'},
        }}
        wrapperStyle={{
          right: {backgroundColor: '#F2BD33'},
          left: {backgroundColor: '#FBDF98', alignSelf: 'flex-start'},
        }}
      />
    );
  }
  renderInputToolbar(props) {
    console.log('this.state.isExpired', this.state.isExpired);
    if (!this.state.isExpired) {
      return <InputToolbar {...props} />;
    }
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <Header title={`#${this.userTo.orderId}`} onBack={this.onBack} />
        <GiftedChat
          messages={this.state.messages}
          isLoadingEarlier={this.state.isLoadingEarlier}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: global.user.firstName,
          }}
          showAvatarForEveryMessage={true}
          alwaysShowSend={true}
          onInputTextChanged={this.onInputTextChanged}
          renderFooter={this.renderFooter}
          renderSend={this.renderSend}
          renderBubble={this.renderBubble}
          renderAvatar={null}
          renderInputToolbar={props => this.renderInputToolbar(props)}
        />
        <Loader isVisibleLoading={this.state.isVisibleLoading} />
      </View>
    );
  }
}
