import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  inner_container: {
    width: wp(96),
    alignSelf: 'center',
  },
  spacing: {
    marginVertical: hp(2.4),
  },
  arrow: {
    height: hp(4.5),
    width: hp(4.5),
  },
  heading_text: {
    fontSize: fonts.FONT_HEADING,
    fontWeight: fonts.FONT_BOLD,
  },
  next_arrow_container: {
    height: hp(5.4),
    width: hp(5.4),
    borderRadius: hp(5.4) / 2,
    backgroundColor: colors.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icons: {
    width: wp(6),
    height: hp(6),
  },
  search_input: {
    width: wp(72),
    padding: 0,
  },
  animation_style: {
    position: 'absolute',
    elevation: 10,
  },
};
export default styles;
