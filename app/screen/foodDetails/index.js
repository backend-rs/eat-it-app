import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Modal,
  ScrollView,
} from 'react-native';
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import Modals from 'react-native-modal';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';
import * as colors from '../../constants/colors';
import * as Service from '../../api/services';
import * as utility from '../../utility/index';
import {NavigationActions, StackActions} from 'react-navigation';
import {WebView} from 'react-native-webview';
import ConfirmPayment from '../confirmPayment';
import ConfirmOrder from '../confirmOrder';
import Loader from '../../components/Loader';
import Header from '../../components/header';
import * as Url from '../../constants/urls';
import AsyncStorage from '@react-native-community/async-storage';

export default class foodDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from: '',
      foodId: '',
      name: '',
      address: '',
      price: 0,
      type: '',
      availabilityTime: 0,
      pickupTime: {
        from: '',
        to: '',
      },
      deliveryPrice: 0,
      totalAmount: 0,
      images: [],
      isVisibleLoading: false,
      isPriceShow: false,
      isDataExists: false,
      userId: '',
      sellerId: '',
      isSeller: true,
      isSkipped: false,
      showModal: false,
      status: 'pending',
      paypalToken: '',
      paymentId: '',
      isDialogVisible: false,
      userToken: '',
      isFreeFoodDialogVisible: false,
      buttonStatus: '',
      showFullSlider: false,
      activeIndex: 0,
      description: '',
      quantity: 0,
      verifiedQuantity: 0,
      foodStatus: 'inActive',
      orderFoodQty: 1,
    };
  }
  componentDidMount = async () => {
    let foodId;
    let from;
    let isSkipped = await utility.getItem('isSkipped');
    const userId = await utility.getItem('userId');
    const token = await utility.getItem('token');

    await this.setState({
      isSkipped: isSkipped,
      userId: userId,
      userToken: token,
    });

    if (this.props.navigation.state.params) {
      if (
        this.props.navigation.state.params.from == 'home' ||
        this.props.navigation.state.params.from == 'search' ||
        this.props.navigation.state.params.from == 'notification' ||
        this.props.navigation.state.params.from == 'outerNotification' ||
        this.props.navigation.state.params.from == 'myFood'
      ) {
        from = this.props.navigation.state.params.from;
        if (this.props.navigation.state.params.foodId) {
          foodId = this.props.navigation.state.params.foodId;
        }

        await this.setState({foodId: foodId, from: from});
        if (this.state.from == 'outerNotification') {
          if (
            this.state.userToken == null ||
            this.state.userToken == undefined ||
            this.state.userToken == ''
          ) {
            await utility.showAlert('Please login first.', this.onLoginFirst);
            await this.props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'BottomTab'})],
              }),
            );
            await this.props.navigation.navigate('tab1', {from: 'foodDetails'});
          }
        }
        await this.getFoodDetail();
      }
    }
  };
  onLoginFirst = async () => {
    await this.props.navigation.navigate('Login');
  };
  getFoodDetail = async () => {
    await this.setState({isVisibleLoading: true});
    try {
      let response = Service.getDataApi(`foods/${this.state.foodId}`, '');
      response
        .then(res => {
          if (res.data) {
            let deliveryPrice;
            let price;
            let pickupTime = {};
            if (
              res.data.homeDeliveryPrice == '' ||
              res.data.homeDeliveryPrice == undefined ||
              res.data.homeDeliveryPrice == null ||
              res.data.homeDeliveryPrice == 0
            ) {
              deliveryPrice = 0;
            } else {
              deliveryPrice = res.data.homeDeliveryPrice;
            }
            if (
              res.data.price == '' ||
              res.data.price == undefined ||
              res.data.price == null ||
              res.data.price == 0
            ) {
              price = 0;
            } else {
              price = res.data.price;
            }
            if (res.data.pickupTime) {
              pickupTime = {
                from: res.data.pickupTime.from,
                to: res.data.pickupTime.to,
              };
            }
            this.setState({
              name: res.data.name,
              address: res.data.address.line,
              price: price,
              type: res.data.type,
              availabilityTime: res.data.availabilityTime,
              pickupTime: pickupTime,
              deliveryPrice: deliveryPrice,
              totalAmount: deliveryPrice + price,
              images: res.data.images,
              sellerId: res.data.userId,
              description: res.data.description,
              quantity: res.data.quantity,
              foodStatus: res.data.status,
            });
            if (this.state.userId == this.state.sellerId) {
              this.setState({isSeller: true});
            } else {
              this.setState({isSeller: false});
            }
            this.setState({
              isVisibleLoading: false,
              isPriceShow: true,
              isDataExists: true,
            });

            console.log('this.state.type', this.state.type);
          } else {
            this.setState({isVisibleLoading: false, isDataExists: false});
            console.log('no data found:', res.error);
            // alert(res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false, isDataExists: false});
          console.log('try-catch error:', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false, isDataExists: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };

  verifyFoodQuantity = async from => {
    await this.setState({isVisibleLoading: true});
    try {
      let response = Service.getDataApi(`foods/${this.state.foodId}`, '');
      response
        .then(res => {
          if (res.data) {
            if (res.data.quantity == 0) {
              utility.alert('Out of stock');
            } else {
              if (from == 'onBuy') {
                this.onGetToken();
              }
              if (from == 'onBuyFreeFood') {
                this.setState({
                  isFreeFoodDialogVisible: true,
                  buttonStatus: 'purchase',
                });
              }
            }
            this.setState({
              isVisibleLoading: false,
            });
          } else {
            this.setState({isVisibleLoading: false});
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('try-catch error:', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };

  onBack = async () => {
    if (this.state.from == 'home') {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'BottomTab'})],
        }),
      );
      await this.props.navigation.navigate('tab1');
    } else if (this.state.from == 'outerNotification') {
      await this.props.navigation.navigate('tab1');
    } else {
      this.props.navigation.goBack();
    }
  };
  get pagination() {
    const {activeSlide} = this.state;
    return (
      <Pagination
        dotsLength={this.state.images.length}
        activeDotIndex={activeSlide}
        dotStyle={styles.dot_style}
        inactiveDotStyle={styles.inactive_dot_styles}
        inactiveDotScale={0.6}
      />
    );
  }
  showFullView = async (item, index) => {
    await this.setState({showFullSlider: true, activeIndex: index});
  };
  _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => this.showFullView(item, index)}>
        <Image
          resizeMode="stretch"
          style={styles.images}
          source={{uri: item.url}}
        />
      </TouchableOpacity>
    );
  };

  onBuy = async () => {
    await this.verifyFoodQuantity('onBuy');
  };

  onAlert = async () => {
    await utility.showAlert('Please login first.', this.onLogin);
  };
  onLogin = async () => {
    await this.props.navigation.navigate('Login');
  };
  handelResponse = async data => {
    if (data.title == 'success') {
      await this.onOrderFood('paid');
    } else if (data.title == 'cancel') {
      await this.setState({showModal: false, status: 'cancelled'});
    } else {
      return;
    }
  };
  onOrderFood = async from => {
    await this.setState({isVisibleLoading: true});
    try {
      let body;
      if (from == 'paid') {
        body = {
          from: from,
          foodId: this.state.foodId,
          paymentId: this.state.paymentId,
          orderQuantity: this.state.orderFoodQty,
        };
      } else {
        body = {
          from: from,
          foodId: this.state.foodId,
        };
        if (this.state.type != 'langar') {
          body.orderQuantity = this.state.orderFoodQty;
        }
      }
      console.log('bodyyyyyyyyyyyy:', body);
      let response = Service.postDataApi(`orders`, body, this.state.userToken);
      response
        .then(res => {
          if (res.data) {
            if (from == 'paid') {
              this.setState({
                showModal: false,
                isDialogVisible: true,
                status: 'completed',
              });
            } else {
              this.closeFreeFoodDialog();
              utility.alert('Order placed successfully');
              this.onBack();
            }
            this.setState({isVisibleLoading: false});
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          this.closeFreeFoodDialog();
          console.log('try-catch error:', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      this.closeFreeFoodDialog();
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onGetToken = async () => {
    await this.setState({isVisibleLoading: true});
    try {
      let response = Service.postPaymentDataApi(
        'payments/token',
        '',
        '',
        'token',
      );
      response
        .then(res => {
          if (res.data) {
            if (res.isSuccess == true) {
              this.setState({isVisibleLoading: false});
              this.setState({paypalToken: res.data.access_token});
              this.onPayment();
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onPayment = async () => {
    await this.setState({isVisibleLoading: true});

    var body = {
      intent: 'sale',
      payer: {
        payment_method: 'paypal',
      },
      redirect_urls: {
        return_url: `${Url.BASE_URL}payments/success?access_token=${
          this.state.paypalToken
        }&&userId=${this.state.userId}&&foodId=${this.state.foodId}`,
        cancel_url: `${Url.BASE_URL}payments/cancel`,
      },
      transactions: [
        {
          item_list: {
            items: [
              {
                name: this.state.name,
                sku: 'item',
                price: this.state.totalAmount * this.state.orderFoodQty,
                currency: 'USD',
                quantity: 1,
              },
            ],
          },
          amount: {
            currency: 'USD',
            total: this.state.totalAmount * this.state.orderFoodQty,
          },
          description: 'This is the payment description.',
        },
      ],
    };
    try {
      let response = Service.postPaymentDataApi(
        `payments/payment?access_token=${this.state.paypalToken}`,
        body,
        '',
        'payment',
      );
      response
        .then(res => {
          if (res.data) {
            if (res.isSuccess == true) {
              this.setState({
                paymentId: res.data.id,
                approvalUrl: res.data.href,
                showModal: true,
                isVisibleLoading: false,
              });
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onBuyFreeFood = async () => {
    await this.verifyFoodQuantity('onBuyFreeFood');
  };
  closeFreeFoodDialog = async () => {
    await this.setState({isFreeFoodDialogVisible: false});
  };
  onPlaceOrder = async () => {
    this.setState({isVisibleLoading: true});
    let body = {
      from: 'free',
      foodId: this.state.foodId,
    };
    try {
      let response = Service.postDataApi(`orders`, body, this.state.userToken);
      response
        .then(res => {
          if (res.data) {
            this.setState({isVisibleLoading: false});
            this.closeFreeFoodDialog();
            utility.alert('Order placed successfully');
            this.props.navigation.navigate('tab1');
          } else {
            this.setState({isVisibleLoading: false});
            this.closeFreeFoodDialog();
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          this.closeFreeFoodDialog();
          console.log('error in try-catch', error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      this.closeFreeFoodDialog();
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  closeDialog = async () => {
    await this.setState({isDialogVisible: false});
    await this.props.navigation.navigate('tab1');
  };
  onCross = async () => {
    await this.setState({showFullSlider: false});
  };
  onIncrement = async () => {
    let quantity = this.state.orderFoodQty;
    if (quantity < this.state.quantity) {
      if (quantity < 9) {
        await this.setState({orderFoodQty: quantity + 1});
      } else {
        await utility.alert(`You can add only 9 items`);
      }
    } else {
      await utility.alert(`Unable to add more quantity`);
    }
  };
  onDecrement = async () => {
    let quantity = this.state.orderFoodQty;
    if (this.state.orderFoodQty > 1) {
      await this.setState({orderFoodQty: quantity - 1});
    }
  };

  render() {
    const {
      button_container,
      content_container_style,
      button_text,
      centered_text,
      scroll_container,
      colored_text,
      detail_text_style,
      type_text,
      price,
      bottom_spacing,
      like_icon,
      product_name,
      detail_container,
      bottom_container,
      row,
      between_spacing,
      top_spacing,
      free_text,
      icons,
      show_full_container,
      horizontal_line,
      description_style,
      total_style,
      quantity_text_style,
      quantity_container,
      add_minus__icon,
    } = styles;
    return (
      <View>
        <Header title={'Food details'} onBack={this.onBack} />
        <View style={{height: hp(88.6)}}>
          <ScrollView>
            {this.state.isDataExists ? (
              <>
                <Carousel
                  layout={'default'}
                  ref={c => {
                    this._carousel = c;
                  }}
                  data={this.state.images}
                  renderItem={this._renderItem}
                  sliderWidth={wp(85)}
                  itemWidth={wp(85)}
                  autoplay={true}
                  firstItem={this.state.activeIndex}
                  onSnapToItem={index => this.setState({activeSlide: index})}
                  containerCustomStyle={scroll_container}
                  contentContainerStyle={content_container_style}
                />
                {this.pagination}

                <View style={[detail_container]}>
                  <View
                    style={
                      this.state.images.length == 1
                        ? [row, between_spacing, bottom_spacing, top_spacing]
                        : [row, between_spacing, bottom_spacing]
                    }>
                    <Text style={product_name}>{this.state.name}</Text>
                  </View>

                  <Text style={[detail_text_style, bottom_spacing]}>
                    {this.state.address}
                  </Text>

                  <View style={[row, between_spacing, bottom_spacing]}>
                    {!this.state.isPriceShow ? (
                      <View />
                    ) : this.state.type == 'langar' ? (
                      <View />
                    ) : this.state.price == 0 ? (
                      <Text style={[price, free_text]}>Free</Text>
                    ) : (
                      <Text style={[price, colored_text]}>
                        Rs {this.state.price}
                      </Text>
                    )}

                    <View style={[row, {alignItems: 'center'}]}>
                      <Image
                        style={icons}
                        source={
                          this.state.type == ''
                            ? ''
                            : this.state.type == 'veg'
                            ? require('../../assets/veg_icon.png')
                            : this.state.type == 'langar'
                            ? require('../../assets/langar.png')
                            : require('../../assets/nonveg_icon.png')
                        }
                      />
                      <Text style={type_text}>{this.state.type}</Text>
                    </View>
                  </View>

                  <View style={horizontal_line} />
                  <View style={description_style}>
                    <Text>{this.state.description}</Text>
                  </View>
                  <View style={horizontal_line} />
                  {this.state.type != 'langar' ? (
                    <View
                      style={[
                        row,
                        bottom_spacing,
                        between_spacing,
                        {marginTop: hp(2)},
                      ]}>
                      <Text style={quantity_text_style}>Quantity : </Text>
                      <View style={[row, {alignItems: 'center'}]}>
                        <TouchableOpacity
                          style={quantity_container}
                          onPress={this.onDecrement}>
                          <Image
                            style={styles.add_minus__icon}
                            source={require('../../assets/minus_white.png')}
                          />
                        </TouchableOpacity>
                        <Text
                          style={[
                            quantity_text_style,
                            {marginHorizontal: hp(1)},
                          ]}>
                          {this.state.orderFoodQty}
                        </Text>
                        <TouchableOpacity
                          style={quantity_container}
                          onPress={this.onIncrement}>
                          <Image
                            style={styles.add_minus__icon}
                            source={require('../../assets/add_white.png')}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  ) : (
                    <View />
                  )}
                  <View style={[row, bottom_spacing, between_spacing]}>
                    <Text style={detail_text_style}>Availability time : </Text>
                    <Text style={detail_text_style}>
                      {this.state.availabilityTime} days
                    </Text>
                  </View>
                  {this.state.type != 'langar' ? (
                    <>
                      <View style={[row, bottom_spacing, between_spacing]}>
                        <Text style={detail_text_style}>
                          Available items :{' '}
                        </Text>
                        <Text style={detail_text_style}>
                          {this.state.quantity} items
                        </Text>
                      </View>
                      <View style={[row, bottom_spacing, between_spacing]}>
                        <Text style={detail_text_style}>Pickup time : </Text>
                        <Text style={detail_text_style}>
                          {this.state.pickupTime.from +
                            ' - ' +
                            this.state.pickupTime.to}
                        </Text>
                      </View>
                      <View style={[row, bottom_spacing, between_spacing]}>
                        <Text style={detail_text_style}>
                          Delivery Charges :{' '}
                        </Text>
                        <Text style={detail_text_style}>
                          Rs{' '}
                          {this.state.deliveryPrice == '' ||
                          this.state.deliveryPrice == undefined
                            ? '0'
                            : this.state.deliveryPrice}
                        </Text>
                      </View>

                      <View style={[row, bottom_spacing, between_spacing]}>
                        <Text style={total_style}>Grand Total : </Text>
                        <Text style={total_style}>
                          Rs {this.state.totalAmount}
                        </Text>
                      </View>
                    </>
                  ) : (
                    <View />
                  )}
                  <Modal
                    visible={this.state.showModal}
                    onRequestClose={() => {
                      this.setState({showModal: false});
                    }}>
                    <WebView
                      style={{height: 400, width: 300}}
                      source={{uri: this.state.approvalUrl}}
                      onNavigationStateChange={data =>
                        this.handelResponse(data)
                      }
                      javaScriptEnabled={true}
                      domStorageEnabled={true}
                      startInLoadingState={false}
                      style={{marginTop: 20}}
                    />
                  </Modal>

                  <View style={[bottom_container, bottom_spacing]}>
                    <Text />
                    {!this.state.isSeller &&
                    this.state.foodStatus == 'active' ? (
                      // && this.state.type !== 'langar'
                      <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={
                          !this.state.isSkipped
                            ? this.state.totalAmount == 0 ||
                              this.state.type == 'langar'
                              ? this.onBuyFreeFood
                              : this.onBuy
                            : this.onAlert
                        }>
                        <LinearGradient
                          start={{x: 0, y: 0}}
                          end={{x: 1, y: 0}}
                          colors={[
                            colors.gradientFirstColor,
                            colors.gradientSecondColor,
                          ]}
                          style={[button_container, centered_text]}>
                          <Text style={button_text}>
                            {this.state.price != 0 ||
                            this.state.type == 'langar'
                              ? 'Buy food'
                              : 'Share food'}
                          </Text>
                        </LinearGradient>
                      </TouchableOpacity>
                    ) : (
                      <View />
                    )}
                  </View>
                </View>

                {this.state.isDialogVisible ? (
                  <ConfirmPayment
                    visible={this.state.isDialogVisible}
                    closeDialog={this.closeDialog}
                  />
                ) : (
                  <View />
                )}
                {this.state.isFreeFoodDialogVisible ? (
                  <ConfirmOrder
                    visible={this.state.isFreeFoodDialogVisible}
                    closeDialog={this.closeFreeFoodDialog}
                    onOrderConfirmation={() => {
                      this.onOrderFood('free');
                    }}
                    status={this.state.buttonStatus}
                  />
                ) : (
                  <View />
                )}
                <Modals
                  backdropOpacity={1}
                  backdropColor={'#DFDEDE'}
                  isVisible={this.state.showFullSlider}
                  hasBackdrop={true}>
                  <TouchableOpacity activeOpacity={0.7} onPress={this.onCross}>
                    <View style={styles.cross_header}>
                      <Image
                        style={styles.cross_icon}
                        source={require('../../assets/cross_icon.png')}
                      />
                    </View>
                  </TouchableOpacity>
                  <Carousel
                    layout={'default'}
                    ref={c => {
                      this._carousel = c;
                    }}
                    data={this.state.images}
                    renderItem={this._renderItem}
                    sliderWidth={wp(94)}
                    itemWidth={wp(94)}
                    autoplay={true}
                    firstItem={this.state.activeIndex}
                    onSnapToItem={index =>
                      this.setState({activeSlide: index, activeIndex: index})
                    }
                    containerCustomStyle={show_full_container}
                    contentContainerStyle={content_container_style}
                  />
                  {this.pagination}
                </Modals>
              </>
            ) : (
              <View />
            )}
          </ScrollView>
        </View>
        <Loader isVisibleLoading={this.state.isVisibleLoading} />
      </View>
    );
  }
}
