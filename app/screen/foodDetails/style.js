import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  button_container: {
    height: hp(5.4),
    width: wp(32),
    borderRadius: 20,
    marginTop: hp(2),
    alignSelf: 'flex-end',
    elevation: 4,
  },
  content_container_style: {
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  button_text: {
    color: 'white',
  },
  centered_text: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  scroll_container: {
    borderColor: colors.primaryColor,
    borderWidth: 1.8,
    borderRadius: 8,
    alignSelf: 'center',
    height: hp(30),
  },
  colored_text: {
    color: colors.primaryColor,
  },
  detail_text_style: {
    fontSize: fonts.FONT_TEXT,
    color: colors.greyText,
  },
  type_text: {
    color: colors.greyText,
    fontSize: 12,
    textTransform: 'capitalize',
  },
  price: {
    fontSize: 18,
    fontWeight: fonts.FONT_BOLD,
  },
  bottom_spacing: {
    marginBottom: hp(2.2),
  },
  like_icon: {
    height: hp(3),
    width: hp(3),
  },
  product_name: {
    fontSize: 20,
    fontWeight: fonts.FONT_BOLD,
    textTransform: 'capitalize',
  },
  detail_container: {
    width: wp(85),
    alignSelf: 'center',
  },
  bottom_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    width: wp(86),
    marginBottom: hp(3),
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  between_spacing: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  top_spacing: {
    marginTop: hp(1),
  },
  free_text: {
    color: 'green',
  },
  icons: {
    height: hp(2),
    width: hp(2),
    marginRight: hp(1),
  },
  show_full_container: {
    borderColor: colors.primaryColor,
    borderWidth: 1.8,
    borderRadius: 8,
    alignSelf: 'center',
    height: hp(86),
  },
  horizontal_line: {
    borderBottomWidth: 1,
    borderColor: colors.greyText,
  },
  description_style: {
    marginVertical: hp(1.6),
  },
  total_style: {
    fontSize: fonts.FONT_HEADING,
  },
  dot_style: {
    width: 15,
    height: 15,
    borderRadius: 15 / 2,
    backgroundColor: colors.primaryColor,
    marginTop: hp(-2.1),
  },
  inactive_dot_styles: {
    backgroundColor: '#696969',
    width: 22,
    height: 22,
    borderRadius: 22 / 2,
  },
  images: {
    height: '100%',
    width: '98.8%',
    borderRadius: 6,
    alignSelf: 'center',
    marginLeft: wp(-1),
  },
  cross_header: {
    marginBottom: hp(3),
    alignSelf: 'flex-end',
  },
  cross_icon: {
    height: hp(3),
    width: hp(3),
  },
  quantity_text_style: {
    fontSize: 16,
  },
  quantity_container: {
    backgroundColor: colors.gradientFirstColor,
    height: hp(2.6),
    width: hp(2.8),
    justifyContent:'center',
    borderRadius:4
  },
  add_minus__icon: {
    height: hp(1.6),
    width: hp(1.6),
    alignSelf:'center'
  },
};
export default styles;
