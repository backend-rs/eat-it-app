import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import styles from './style';
import Header from '../../components/header';

export default class about extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  onBack = async () => {
    await this.props.navigation.goBack();
  };

  render() {
    const {
      sub_heading,
      row,
      right_container,
      content_container,
      content_style,
      left_container,
    } = styles;
    return (
      <>
        <Header title={'About Us'} onBack={this.onBack} />
        <View style={[content_container, row]}>
          <Image
            style={left_container}
            source={require('../../assets/fork_leaves.png')}
          />
          <View style={right_container}>
            <Text style={sub_heading}>Eat it{'\n'}</Text>
            <Text style={content_style}>
              Wasting food is bad, and destructive for the Earth’s environment.
              {'\n'}
              {'\n'}
              EAT it trying to stop wastage of food, and help them who need this
              food.{'\n'}
              {'\n'}
              EAT IT’s mission is to raise awareness of food waste by making
              surplus food available to its users.{'\n'}
              {'\n'}
              We provide a way for the user (”Customer”) to place his or her
              reservation of surplus food so-called ”Langar” (”Free food”) from
              homes, shops, restaurants, hotels, etc. {'\n'}
              {'\n'}
              And we provide the platform for who they can make food from home
              and sell this from home at a reasonable price to all who need
              food.
            </Text>
          </View>
        </View>
      </>
    );
  }
}
