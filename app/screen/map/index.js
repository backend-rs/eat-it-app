import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Dimensions,
  ScrollView,
} from 'react-native';
import styles from './style';
import MapView from 'react-native-maps';
import {heightPercentageToDP} from '../../utility';
import LinearGradient from 'react-native-linear-gradient';
import * as colors from '../../constants/colors';
import {PermissionsAndroid} from 'react-native';
import Geocoder from 'react-native-geocoding';
import Geolocation from '@react-native-community/geolocation';
const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import * as Service from '../../api/services';
import * as utility from '../../utility/index';
import {TextInputMask} from 'react-native-masked-text';
import Loader from '../../components/Loader';

export default class map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 29.036096894770765, // 28.605251560868886
        longitude: 75.72711573913693, //77.05620612949133
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      coordinate: {
        latitude: 29.0552648,   // 28.605251560868886
        longitude: 75.725159,    //77.05620612949133
      },
      addressLine: '',
      landmark: '',
      city: '',
      pincode: '',
      loginType: '',
      phone: '',
      from: '',
      isAddressExists: false,
      userId: '',
      userToken: '',
      isSkipped: false,
      isVisibleLoading: false,
    };
    Geocoder.init('AIzaSyAr_cT485DGoQBlnD9K2qDhLl2u0CXET2Y');
    // Geocoder.init('AIzaSyCgd-rD47NFwKVpQ30skw_D-qWUMHrxjO4'); //for testing
  }
  componentWillMount() {
    this.requestCameraPermission();
    this.onRegionChange();
  }
  componentDidMount = async () => {
    await this.onRegionChange();
    let loginType = await utility.getItem('loginType');
    let isSkipped = await utility.getItem('isSkipped');
    const userId = await utility.getItem('userId');
    const userToken = await utility.getToken('token');
    let isAddressExists = await utility.getItem('isAddressExists');
    await this.setState({
      isSkipped: isSkipped,
      userId: userId,
      userToken: userToken,
      loginType: loginType,
      isAddressExists: isAddressExists,
    });
    if (
      this.props.navigation.state.params &&
      this.props.navigation.state.params.from
    ) {
      await this.setState({from: this.props.navigation.state.params.from});
    }
  };
  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'App location Permission',
          message: ' App needs access to your location ' + '',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the loc');
      } else {
        console.log('loc permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };
  onRegionChange = async () => {
    Geolocation.getCurrentPosition(
      position => {
        this.onUserPinDragEnd(position.coords);
        console.log('position.coords', position.coords);
      },

      error => {
        this.setState({error: error.message}),
          console.log('errorrrrrrrrr:', error.message);
      },
      {enableHighAccuracy: false, timeout: 200000, maximumAge: 10000},
    );
  };
  onUserPinDragEnd = async value => {
    let address = await Geocoder.from({
      latitude: value.latitude,
      longitude: value.longitude,
    });
    var addressComponent = address.results[0].address_components;
    var formattedAddress = address.results[0].formatted_address;
    await this.setState({
      addressLine: formattedAddress,
      region: {
        latitude: value.latitude,
        longitude: value.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      coordinate: {
        latitude: value.latitude,
        longitude: value.longitude,
      },
      landmark: addressComponent[addressComponent.length - 4].long_name,
      city: addressComponent[addressComponent.length - 4].long_name,
      pincode: addressComponent[addressComponent.length - 1].long_name,
    });
    console.log('coordinatesssss::', this.state.coordinate);
  };
  onAddressStatusUpdate = async () => {
    await utility.setItem('isAddressExists', this.state.isAddressExists);
    await this.props.navigation.goBack();
    await this.props.navigation.state.params.refersh();
  };
  onAdd = async () => {
    if (this.state.isSkipped) {
      let coordinates = {
        longitude: this.state.region.longitude,
        latitude: this.state.region.latitude,
      };
      await utility.setItem('coordinates', coordinates);
      await this.props.navigation.goBack();
      await this.props.navigation.state.params.refersh();
    } else {
      if (this.state.from == 'addFood' || this.state.loginType == 'app') {
        if (
          utility.isFieldEmpty(
            this.state.addressLine &&
              this.state.landmark &&
              this.state.city &&
              this.state.pincode &&
              this.state.region.latitude &&
              this.state.region.longitude,
          )
        ) {
          utility.alert('All fields are required');
        } else {
          let address = {
            line: this.state.addressLine,
            landmark: this.state.landmark,
            city: this.state.city,
            pincode: this.state.pincode,
            location: {
              type: 'Point',
              coordinates: [
                this.state.region.longitude,
                this.state.region.latitude,
              ],
            },
          };
          if (this.state.loginType == 'app' && this.state.from == 'home') {
            await this.onUserUpdate(address, 'app');
          }
          if (this.state.from == 'addFood') {
            await this.props.navigation.goBack();
            await this.props.navigation.state.params.refersh(address);
          }
        }
      } else {
        if (
          utility.isFieldEmpty(
            this.state.addressLine &&
              this.state.landmark &&
              this.state.city &&
              this.state.pincode &&
              this.state.phone &&
              this.state.region.latitude &&
              this.state.region.longitude,
          )
        ) {
          utility.alert('All fields are required');
        } else {
          let data = {
            phone: this.state.phone,
            address: {
              line: this.state.addressLine,
              landmark: this.state.landmark,
              city: this.state.city,
              pincode: this.state.pincode,
              location: {
                type: 'Point',
                coordinates: [
                  this.state.region.longitude,
                  this.state.region.latitude,
                ],
              },
            },
          };
          await this.onUserUpdate(data, 'other');
        }
      }
    }
  };
  onUserUpdate = async (data, from) => {
    await this.setState({isVisibleLoading: true});
    let body;
    if (from == 'app') {
      body = {
        address: data,
      };
    } else {
      body = {
        phone: data.phone,
        address: data.address,
      };
    }
    try {
      let response = Service.putDataApi(
        `users/${this.state.userId}`,
        body,
        this.state.userToken,
      );
      response
        .then(res => {
          if (res.data) {
            if (res.data.address) {
              if (
                res.data.address.line == undefined ||
                res.data.address.line == null ||
                res.data.address.line == ''
              ) {
                this.setState({isAddressExists: false});
              } else {
                this.setState({isAddressExists: true});
              }
            }
            this.onAddressStatusUpdate();
            this.setState({isVisibleLoading: false});
            utility.alert('Location  update successfully');
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  render() {
    const {
      container,
      row,
      between_spacing,
      fields,
      input_box,
      small_input_box,
      address_fields_container,
      centered_text,
      button_container,
      button_text,
    } = styles;
    return (
      <ScrollView>
        <View style={container}>
          <MapView
            style={{height: heightPercentageToDP(65)}}
            showsUserLocation={true}
            followsUserLocation={true}
            scrollEnabled={true}
            zoomEnabled={true}
            initialRegion={this.state.region}
            showsIndoors={true}
            onRegionChangeComplete={value => {
              this.onUserPinDragEnd(value);
            }}
            zoom={20}>
            <MapView.Marker
              coordinate={this.state.coordinate}
              title={'title'}
              description={'description'}
            />
          </MapView>
          <View style={address_fields_container}>
            {this.state.loginType != 'app' && this.state.from != 'addFood' ? (
              <View style={[row, fields]}>
                <TextInputMask
                  require
                  allowFontScaling={false}
                  require
                  placeholder="Phone"
                  type={'custom'}
                  options={{
                    mask: '9999999999',
                  }}
                  keyboardType="numeric"
                  onChangeText={phone => this.setState({phone})}
                  value={this.state.phone}
                  style={input_box}
                />
              </View>
            ) : (
              <View />
            )}
            <View style={[row, fields]}>
              <TextInput
                placeholder="Address line"
                style={input_box}
                onChangeText={addressLine => this.setState({addressLine})}
                value={this.state.addressLine}
              />
            </View>
            <View style={[row, fields]}>
              <TextInput
                placeholder="Landmark"
                style={input_box}
                onChangeText={landmark => this.setState({landmark})}
                value={this.state.landmark}
              />
            </View>
            <View style={[row, between_spacing]}>
              <View style={[row, fields]}>
                <TextInput
                  placeholder="City"
                  style={small_input_box}
                  onChangeText={city => this.setState({city})}
                  value={this.state.city}
                />
              </View>
              <View style={[row, fields]}>
                <TextInput
                  placeholder="Pincode"
                  style={small_input_box}
                  onChangeText={pincode => this.setState({pincode})}
                  value={this.state.pincode}
                />
              </View>
            </View>
          </View>
          <View>
            <TouchableOpacity activeOpacity={0.7} onPress={this.onAdd}>
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={[colors.gradientFirstColor, colors.gradientSecondColor]}
                style={[button_container, centered_text]}>
                <Text style={button_text}>Add address</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
          <Loader isVisibleLoading={this.state.isVisibleLoading} />
        </View>
      </ScrollView>
    );
  }
}
