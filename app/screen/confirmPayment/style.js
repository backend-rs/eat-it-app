import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  container: {
    width: '100%',
    height: '100%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  around_spacing: {justifyContent: 'space-around'},
  button_container: {
    alignSelf: 'flex-end',
    justifyContent: 'space-between',
    width: wp(60),
    alignItems: 'center',
  },
  bottom_margin: {
    marginBottom: hp(4),
  },
  vertical_margin: {
    paddingVertical: hp(2),
    paddingHorizontal: wp(4),
  },
  dialog_container: {
    width: wp(88),
    elevation: 6,
    backgroundColor: '#ECECEC',
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  text_style: {
    fontSize: fonts.FONT_TEXT,
    fontWeight: fonts.FONT_BOLD,
    textAlign: 'center',
    lineHeight: hp(3.4),
  },
  button: {
    height: hp(5.4),
    width: wp(32),
    borderRadius: 20,
    alignSelf: 'flex-end',
    elevation: 4,
  },
  colored_text: {
    color: colors.primaryColor,
  },
  button_text: {
    color: 'white',
  },
  centered_text: {
    justifyContent: 'center',
    alignItems: 'center',
  },
};
export default styles;
