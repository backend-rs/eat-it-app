import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  right_container: {
    width: wp(50),
  },
  left_container: {
    width: wp(38),
  },
  content_style: {
    fontSize: 13,
    fontStyle: 'italic'
  },
  sub_heading: {
    marginTop:hp(4),
    fontSize: 20,
    fontWeight:'bold',
    color: '#F58F46',
  },
  content_container: {
    width: wp(92.8),
    alignSelf: 'center',
  },
};
export default styles;
