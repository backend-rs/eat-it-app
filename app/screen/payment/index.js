import React, {Component} from 'react';
import {Text, NativeModules} from 'react-native';
import styles from './style';
import Header from '../../components/header';
import {TouchableOpacity} from 'react-native-gesture-handler';
import * as Service from '../../api/services';
import * as utility from '../../utility/index';

const AllInOneSDKManager = NativeModules.AllInOneSDKManager;

export default class payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderId: 'ORDERID_61011',
      txnAmount: {
        value: '1.00',
        currency: 'INR',
      },
      userInfo: {
        custId: 'CUST_001',
      },
    };
  }
  onBack = async () => {
    await this.props.navigation.goBack();
  };
  getToken = async () => {
    try {
      let body = {
        for: 'paytmToken',
        orderId: this.state.orderId,
        txnAmount: this.state.txnAmount,
        userInfo: this.state.userInfo,
      };
      let response = Service.postDataApi(
        `payments/paytmInitiateTokenOrStatus`,
        body,
        '',
      );
      response
        .then(res => {
          if (res.data) {
            console.log('token response', res.data);
            if (res.data.body) {
              if (res.data.body.txnToken) {
                this.onPayment(res.data);
              }
            }
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onPayment = async data => {
    let mid = 'TPByuh63028345942141';

    // IsStaging is to define staging or production server (True for staging and False for production)
    let isStaging = true;

    // Staging callbackurl
    let callbackurl = `https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=${
      this.state.orderId
    }`;
    // Production callbackurl
    // let callbackurl = `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${
    //   this.state.orderId
    // }`;

    const displayResult = result => {
      this.setShowToast(result);
    };
    // start transition
    await AllInOneSDKManager.startTransaction(
      this.state.orderId,
      mid,
      data.body.txnToken,
      this.state.txnAmount.value,
      callbackurl,
      isStaging,
      displayResult,
    );
  };

  setShowToast = async result => {
    console.log('resuttttttttttttt', result);
    if (result) {
      await this.createPayment(JSON.parse(result));
    }
  };

  createPayment = async data => {
    try {
      let body = data;
      let response = Service.postDataApi(`payments/createPayment`, body, '');
      response
        .then(res => {
          if (res.data) {
            console.log('createPayment response', res.data);
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  render() {
    const {} = styles;
    return (
      <>
        <Header title={'Payment'} onBack={this.onBack} />
        <TouchableOpacity
          onPress={this.getToken}
          style={{backgroundColor: 'red', width: 100, marginVertical: 40}}
          activeOpacity={0.7}>
          <Text>Payment token</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: 'skyblue', width: 200}}
          activeOpacity={0.7}>
          <Text>Payment money</Text>
        </TouchableOpacity>
      </>
    );
  }
}
