import * as fonts from '../../constants/fonts';
import * as colors from '../../constants/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../utility/index';

const styles = {
  container: {
    width: '100%',
    height: '92%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  between_spacing: {
    justifyContent: 'space-between',
  },
  centered_text: {justifyContent: 'center', alignItems: 'center'},
  spacing: {
    marginTop: hp(2.4),
    // marginBottom: hp(3.8),
  },
  profile_image: {
    height: hp(5.8),
    width: hp(5.8),
    borderRadius: hp(5.8) / 2,
    alignSelf: 'center',
    borderColor: colors.primaryColor,
    borderWidth: 1.4,
    marginRight: hp(5),
  },
  arrow_icon: {
    height: hp(2.2),
    width: hp(2.2),
    alignSelf: 'flex-end',
  },
  bottom_margin: {
    marginBottom: hp(3.6),
  },
  name_heading: {
    fontSize: fonts.FONT_TEXT,
    fontWeight: fonts.FONT_BOLD,
  },
  time_style: {
    color: colors.greyText,
    fontSize: fonts.FONT_NORMAL,
  },
  heading_width: {
    width: wp(61.2),
  },
  icons_style: {
    height: hp(3.2),
    width: hp(3.2),
  },
  colored_text: {
    color: colors.primaryColor,
  },
  list_height: {
    height: hp(86),
  },
};
export default styles;
