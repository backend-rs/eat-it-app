import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import styles from './style';
import {NavigationActions, StackActions} from 'react-navigation';
import * as Service from '../../api/services';
import * as utility from '../../utility/index';
import Loader from '../../components/Loader';
import Header from '../../components/header';

export default class notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisibleLoading: false,
      userToken: '',
      from: '',
      notifications: [],
      dataExist: true,
    };
  }

  componentDidMount = async () => {
    const token = await utility.getToken('token');
    const userId = await utility.getItem('userId');
    await this.setState({userToken: token, userId: userId});

    if (this.props.navigation.state.params) {
      let from;
      if (this.props.navigation.state.params.from) {
        from = this.props.navigation.state.params.from;
        await this.setState({from: from});
      }
    } else {
      await this.setState({from: ''});
    }
    await this.getNotifications();
  };

  getNotifications = async () => {
    await this.setState({isVisibleLoading: true});

    try {
      let response = Service.getDataApi(
        `pushNotification?userId=${this.state.userId}`,
        this.state.userToken,
      );
      response
        .then(res => {
          if (res.data) {
            if (res.data.length != 0) {
              let tempNotifications = [];
              let time;
              for (let item of res.data) {
                if (item) {
                  let title;
                  if (item.notification && item.notification.title) {
                    title = item.notification.title;
                  }
                  if (
                    title == 'New food' ||
                    title == 'Order delivered' ||
                    title == 'Order received' ||
                    title == 'Order confirmed'
                  ) {
                    tempNotifications.push({
                      id: item._id,
                      notification: item.notification,
                      data: item.data,
                      orderCode: item.orderCode,
                      orderId: item.orderId,
                      time: time,
                      foodId: item.foodId,
                    });
                  }
                }
              }
              this.setState({
                isVisibleLoading: false,
                notifications: tempNotifications,
                dataExist: true,
              });
            } else {
              this.setState({dataExist: false, isVisibleLoading: false});
              console.log('no data found');
            }
          } else {
            this.setState({dataExist: false, isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({dataExist: false, isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({dataExist: false, isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onBack = async () => {
    if (this.state.from == 'home') {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'BottomTab'})],
        }),
      );
      await this.props.navigation.navigate('tab1');
      // await this.props.navigation.navigate('Home');
    }
    if (this.state.from == 'profile') {
      await this.props.navigation.navigate('tab5');
      // await this.props.navigation.navigate('Profile');
    }
  };
  getOrderDetail = async orderId => {
    await this.setState({isVisibleLoading: true});
    try {
      let response = Service.getDataApi(
        `orders/${orderId}`,
        this.state.userToken,
      );
      response
        .then(res => {
          if (res.data) {
            this.setState({status: res.data.status});
          } else {
            this.setState({isVisibleLoading: false});
            console.log('no data found', res.error);
          }
        })
        .catch(error => {
          this.setState({isVisibleLoading: false});
          console.log('error in try-catch', error.error);
          utility.alert('Something went wrong');
        });
    } catch (err) {
      this.setState({isVisibleLoading: false});
      console.log('another problem:', err);
      utility.alert('Something went wrong');
    }
  };
  onListItem = async (data, foodId, orderId, isToday) => {
    let from = data.notificationType;

    if (from == 'New food') {
      await this.props.navigation.navigate('FoodDetails', {
        from: 'notification',
        foodId: foodId,
      });
    } else if (from == 'Order delivered') {
      await this.props.navigation.navigate('OrderDetails', {
        from: 'notification',
        for: 'delivered',
        orderId: orderId,
        status: 'delivered',
      });
    } else if (from == 'Order received') {
      await this.props.navigation.navigate('OrderDetails', {
        from: 'notification',
        for: 'pending',
        orderId: orderId,
        status: 'pending',
      });
    } else {
      await this.props.navigation.navigate('OrderDetails', {
        from: 'notification',
        for: 'confirmed',
        orderId: orderId,
        status: 'confirmed',
      });
    }
  };

  render() {
    const {
      container,
      column,
      list_height,
      row,
      between_spacing,
      centered_text,
      spacing,
      profile_image,
      arrow_icon,
      bottom_margin,
      name_heading,
      time_style,
      colored_text,
      heading_width,
      icons_style,
    } = styles;
    return (
      <>
        <Header title={'Notifications'} onBack={this.onBack} />
        <View style={[container, column, between_spacing, spacing]}>
          <View>
            {this.state.dataExist ? (
              <View style={list_height}>
                <ScrollView>
                  {this.state.notifications.map(value => {
                    return (
                      <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() =>
                          this.onListItem(
                            value.data,
                            value.foodId,
                            value.orderId,
                            value,
                          )
                        }>
                        <View
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <View style={[row, between_spacing, bottom_margin]}>
                            <View style={[profile_image, centered_text]}>
                              <Image
                                resizeMode="cover"
                                style={icons_style}
                                source={
                                  value.notification.title == 'New food'
                                    ? require('../../assets/new_food.png')
                                    : value.notification.title ==
                                      'Order delivered'
                                    ? require('../../assets/delivered.png')
                                    : value.notification.title ==
                                      'Order confirmed'
                                    ? require('../../assets/confirmed.png')
                                    : require('../../assets/dish_yellow.png')
                                }
                              />
                            </View>
                            {value.notification.title == 'New food' ? (
                              <View style={[row, heading_width]}>
                                <Text style={[name_heading]}>
                                  {value.notification.body}
                                </Text>
                              </View>
                            ) : (
                              <View
                                style={[
                                  row,
                                  heading_width,
                                  {flexWrap: 'wrap'},
                                ]}>
                                <Text style={name_heading}>
                                  Order no.
                                  <Text style={colored_text}>
                                    {' '}
                                    #{value.orderCode}
                                  </Text>{' '}
                                  has been{' '}
                                  {value.notification.title == 'Order delivered'
                                    ? 'delivered'
                                    : value.notification.title ==
                                      'Order confirmed'
                                    ? 'confirmed'
                                    : 'received'}
                                </Text>
                              </View>
                            )}
                            <View style={[column, between_spacing]}>
                              <Image
                                resizeMode="cover"
                                style={arrow_icon}
                                source={require('../../assets/next_arrow_grey.png')}
                              />
                              <Text style={time_style}>{value.time}</Text>
                            </View>
                          </View>
                        </View>
                      </TouchableOpacity>
                    );
                  })}
                </ScrollView>
              </View>
            ) : (
              <View style={[list_height, column, centered_text]}>
                <Text style={{textAlign: 'center'}}>
                  No notifications found
                </Text>
              </View>
            )}
            <Loader isVisibleLoading={this.state.isVisibleLoading} />
          </View>
        </View>
      </>
    );
  }
}
